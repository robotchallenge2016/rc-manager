var gulp = require("gulp");
var concat = require("gulp-concat");
var rename = require("gulp-rename");
var uglify = require("gulp-uglify");
var runSequence = require("run-sequence");
var sass = require("gulp-sass");
var browserSync = require("browser-sync");
var shell = require('gulp-shell');
var util = require('gulp-util');

gulp.task("login.lib.min.js", function () {
    return gulp.src([
        "bower_components/angular/angular.js",
        "bower_components/angular-route/angular-route.min.js",
        "bower_components/angular-ui-validate/dist/validate.min.js",
        "bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js",
        "bower_components/angular-loading-bar/build/loading-bar.min.js",
        "bower_components/jquery/dist/jquery.min.js",
        "bower_components/bootstrap/dist/js/bootstrap.min.js"
    ])
    .pipe(concat("lib.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/login/js"));
});

gulp.task("logout.lib.min.js", function () {
    return gulp.src([
        "bower_components/angular/angular.min.js",
        "bower_components/angular-loading-bar/build/loading-bar.min.js",
        "bower_components/jquery/dist/jquery.min.js",
        "bower_components/bootstrap/dist/js/bootstrap.min.js"
    ])
    .pipe(concat("lib.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/logout/js"));
});

gulp.task("panel.lib.min.js", function () {
    return gulp.src([
        "bower_components/angular/angular.js",
        "bower_components/angular-route/angular-route.min.js",
        "bower_components/angular-animate/angular-animate.min.js",
        "bower_components/angular-sanitize/angular-sanitize.min.js",
        "bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js",
        "bower_components/angular-loading-bar/build/loading-bar.min.js",
        "bower_components/ui-select/dist/select.min.js",
        "bower_components/angular-ui-validate/dist/validate.min.js",
        "bower_components/jquery/dist/jquery.min.js",
        "bower_components/jquery-ui/jquery-ui.min.js",
        "bower_components/bootstrap/dist/js/bootstrap.min.js",
        "bower_components/angular-ui-tinymce/src/tinymce.js",
        "bower_components/angular-ui-grid/ui-grid.min.js"
    ])
    .pipe(concat("lib.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/panel/js"));
});

gulp.task("gate.lib.min.js", function () {
    return gulp.src([
        "bower_components/angular/angular.js",
        "bower_components/angular-route/angular-route.min.js",
        "bower_components/angular-animate/angular-animate.min.js",
        "bower_components/angular-sanitize/angular-sanitize.min.js",
        "bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js",
        "bower_components/angular-loading-bar/build/loading-bar.min.js",
        "bower_components/ui-select/dist/select.min.js",
        "bower_components/angular-ui-validate/dist/validate.min.js",
        "bower_components/jquery/dist/jquery.min.js",
        "bower_components/jquery-ui/jquery-ui.min.js",
        "bower_components/bootstrap/dist/js/bootstrap.min.js"
    ])
    .pipe(concat("lib.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/gate/js"));
});

gulp.task("login.app.min.js", function () {
    return gulp.src([
        "src/login/js/**/*.js",
        "src/commons/js/**/*.js"
    ])
    .pipe(concat("app.min.js"))
    // .pipe(uglify())
    .pipe(gulp.dest("dist/login/js"));
});

gulp.task("logout.app.min.js", function () {
    return gulp.src([
        "src/logout/js/**/*.js",
        "src/commons/js/**/*.js"
    ])
    .pipe(concat("app.min.js"))
    // .pipe(uglify())
    .pipe(gulp.dest("dist/logout/js"));
});

gulp.task("panel.app.min.js", function () {
    return gulp.src([
        "src/panel/js/**/*.js",
        "src/commons/js/**/*.js"
    ])
    .pipe(concat("app.min.js"))
    // .pipe(uglify())
    .pipe(gulp.dest("dist/panel/js"));
});

gulp.task("gate.app.min.js", function () {
    return gulp.src([
        "src/gate/js/**/*.js",
        "src/commons/js/**/*.js"
    ])
    .pipe(concat("app.min.js"))
    // .pipe(uglify())
    .pipe(gulp.dest("dist/gate/js"));
});

gulp.task("login.lib.min.css", function () {
    return gulp.src([
        "bower_components/angular-loading-bar/build/loading-bar.css",
        "bower_components/bootstrap/dist/css/bootstrap.min.css",
        "bower_components/font-awesome/css/font-awesome.min.css"
    ])
    .pipe(concat("lib.min.css"))
    .pipe(gulp.dest("dist/login/css"));
});

gulp.task("logout.lib.min.css", function () {
    return gulp.src([
        "bower_components/angular-loading-bar/build/loading-bar.css",
        "bower_components/bootstrap/dist/css/bootstrap.min.css",
        "bower_components/font-awesome/css/font-awesome.min.css"
    ])
    .pipe(concat("lib.min.css"))
    .pipe(gulp.dest("dist/logout/css"));
});

gulp.task("panel.lib.min.css", function () {
    return gulp.src([
        "bower_components/angular-loading-bar/build/loading-bar.css",
        "bower_components/bootstrap/dist/css/bootstrap.min.css",
        "bower_components/ui-select/dist/select.min.css",
        "bower_components/font-awesome/css/font-awesome.min.css",
        "bower_components/angular-ui-grid/ui-grid.min.css"
    ])
    .pipe(concat("lib.min.css"))
    .pipe(gulp.dest("dist/panel/css"));
});

gulp.task("gate.lib.min.css", function () {
    return gulp.src([
        "bower_components/angular-loading-bar/build/loading-bar.css",
        "bower_components/bootstrap/dist/css/bootstrap.min.css",
        "bower_components/ui-select/dist/select.min.css",
        "bower_components/font-awesome/css/font-awesome.min.css",
        "bower_components/angular-ui-grid/ui-grid.min.css"
    ])
    .pipe(concat("lib.min.css"))
    .pipe(gulp.dest("dist/gate/css"));
});

gulp.task("login.app.min.css", function () {
    return gulp.src([
        "src/commons/css/**/*.css",
        "src/login/css/**/*.css"
    ])
    .pipe(concat("app.min.css"))
    .pipe(gulp.dest("dist/login/css"));
});

gulp.task("logout.app.min.css", function () {
    return gulp.src([
        "src/commons/css/**/*.css",
        "src/logout/css/**/*.css"
    ])
    .pipe(concat("app.min.css"))
    .pipe(gulp.dest("dist/logout/css"));
});

gulp.task("panel.app.min.css", function () {
    return gulp.src([
        "src/commons/css/**/*.css",
        "src/panel/css/**/*.css"
    ])
    .pipe(concat("app.min.css"))
    .pipe(gulp.dest("dist/panel/css"));
});

gulp.task("gate.app.min.css", function () {
    return gulp.src([
        "src/commons/css/**/*.css",
        "src/gate/css/**/*.css"
    ])
    .pipe(concat("app.min.css"))
    .pipe(gulp.dest("dist/gate/css"));
});

gulp.task("fonts", function () {
    return gulp.src([
        "bower_components/bootstrap/dist/fonts/*",
        "bower_components/font-awesome/fonts/*",
        "bower_components/angular-ui-grid/*.eot",
        "bower_components/angular-ui-grid/*.svg",
        "bower_components/angular-ui-grid/*.ttf",
        "bower_components/angular-ui-grid/*.woff"
    ])
    .pipe(gulp.dest("dist/fonts"));
});

gulp.task("fontsInCss", function () {
    return gulp.src([
        "bower_components/angular-ui-grid/*.eot",
        "bower_components/angular-ui-grid/*.svg",
        "bower_components/angular-ui-grid/*.ttf",
        "bower_components/angular-ui-grid/*.woff"
    ])
    .pipe(gulp.dest("dist/panel/css"));
});

gulp.task("update-container", function () {
    return gulp.src([
        "gulpfile.js"
    ])
    .pipe(shell([
        "./update",
    ]));
});

gulp.task("html-watch", ["update-container"], browserSync.reload);
gulp.task("js-watch", ["login.app.min.js", "logout.app.min.js", "panel.app.min.js", "gate.app.min.js"], function() {
    runSequence("update-container", browserSync.reload);
});
gulp.task("css-watch", ["login.app.min.css", "logout.app.min.css", "panel.app.min.css", "gate.app.min.css"], function() {
    runSequence("update-container", browserSync.reload);
});

gulp.task("watch", function () {
    var proxy = "https://localhost";
    if(util.env.host) {
        proxy = util.env.host;
    }
    browserSync.init({
        proxy: proxy,
        browser: "google chrome",
        open: false,
        options: {
            "no-open": true
        }
    });

    gulp.watch("src/**/js/**/*.js", ["js-watch"]);
    gulp.watch("src/**/css/**/*.css", ["css-watch"]);
    gulp.watch("src/**/views/**/*.html", ["html-watch"]);
    gulp.watch("src/**/index.html", ["html-watch"]);
});

gulp.task("gate", function () {
    runSequence("gate.lib.min.js", "gate.app.min.js", "gate.lib.min.css", "gate.app.min.css", "fonts", "fontsInCss");
});

gulp.task("default", function () {
    runSequence("login.lib.min.js", "logout.lib.min.js", "panel.lib.min.js", "gate.lib.min.js",
                "login.app.min.js", "logout.app.min.js", "panel.app.min.js", "gate.app.min.js",
                "login.lib.min.css", "logout.lib.min.css", "panel.lib.min.css", "gate.lib.min.css",
                "login.app.min.css", "logout.app.min.css", "panel.app.min.css", "gate.app.min.css",
                "fonts", "fontsInCss"
            );
});
