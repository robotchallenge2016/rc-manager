app.controller("logoutCtrl", ["$scope", function ($scope) {

	// Preloader
    $scope.isInitialized = false;

	$scope.appReady = function () {
        $scope.isInitialized = true;
    };	
}]);
