var app = angular.module("app", ["ngRoute", "ui.validate", "ui.bootstrap", "angular-loading-bar"]);

app.config(["$httpProvider", function ($httpProvider) {
    $httpProvider.interceptors.push([ "$injector", function ($injector) {
            return $injector.get("applicationErrorInterceptor");
        }
    ]);
}]);

app.run(['$route', function($route)  {
  $route.reload();
}]);
