app.controller("verificationFormCtrl", [
    "$scope", "$location", "$routeParams", "verificationService",
    function($scope, $location, $routeParams, verificationService) {
    $scope.verificationInProgress = false;
    $scope.verified = false;
    $scope.verificationFailed = false;

    verify();

    function verify() {
        $scope.verificationInProgress = true;
        verificationService.verify($routeParams.hash)
        .then(function(result) {
            $scope.verificationInProgress = false;
            $scope.verified = true;
        })
        .catch(function(error) {
            $scope.verificationInProgress = false;
            $scope.verificationFailed = true;
        });
    }
}]);
