app.service("verificationService", ["$http", function ($http) {

	this.verify = function(hash) {
		return $http.post("/rc-webapp/v1/tournament/verification/hash/" + encodeURIComponent(hash));
	};
}]);
