app.config(["$routeProvider", function ($routeProvider) {
	$routeProvider

	.when("/", {
		redirectTo: "/login"
	})

	// Home page
	.when("/registration", {
		templateUrl: "views/registration.view.html",
		controller: "registrationFormCtrl"
	})
	.when("/registered", {
		templateUrl: "views/registered.view.html",
		controller: "registrationFormCtrl"
	})
	.when("/verification", {
		templateUrl: "views/verification.view.html",
		controller: "verificationFormCtrl"
	})
	.when("/login", {
		templateUrl: "views/login.view.html",
		controller: "loginFormCtrl"
	})
	// Not found page
	.otherwise({
		templateUrl: "views/core/navigation/not-found.view.html"
	});
}]);
