app.service("registrationService", ["$http", "$q", function ($http, $q) {

	this.create = function(competitor) {
		return $http.post("/rc-webapp/v1/tournament/registration", competitor);
	};

	this.loginIsFree = function(login) {
		if(login !== null) {
			return $http.get("/rc-webapp/v1/tournament/registration/login/" + login + "/free");
		}
		return $q.when(true);
	};
}]);
