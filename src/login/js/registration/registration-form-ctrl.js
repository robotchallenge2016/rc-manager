app.controller("registrationFormCtrl", ["$scope", "$location", "alertService", "registrationService",
	function ($scope, $location, alertService, registrationService) {

	var competitor = {};
	$scope.competitor = competitor;

	$scope.save = function(competitor) {
		saveInProgress(true);

		var data = angular.copy(competitor);
		delete data.confirmPassword;

		registrationService.create(data)
		.then(function(result) {
			alertService.success("Konto zostało utworzone !");
			$location.path("/registered");
			saveInProgress(false);
		})
		.catch(function(error) {
			saveInProgress(false);
		});
	};

	$scope.competitorLoginIsFree = function(login) {
		return registrationService.loginIsFree(login);
	};

	function saveInProgress(show) {
		$scope.inProgress = show;
	}
}]);
