app.controller("loginAppCtrl", [
	"$scope", "$location", "$routeParams", "$window", "sessionService", "alertService",
	function ($scope, $location, $routeParams, $window, sessionService, alertService) {

	// Preloader
    $scope.isInitialized = false;
	$scope.inProgress = true;

	$scope.alerts = alertService.alerts();
	$scope.closeAlert = alertService.closeAlert;

	sessionService.introspection()
	.then(function(result) {
		var encodedTargetUrl = $location.search().TARGET_URL;
		if(encodedTargetUrl) {
			var unencodedTargetUrl = atob(encodedTargetUrl);
			$window.location = unencodedTargetUrl;
		} else {
			$window.location.href = '/panel/';
		}
	})
	.catch(function(error) {
		$scope.inProgress = false;
	});

    $scope.appReady = function () {
        $scope.isInitialized = true;
    };

    $scope.getClass = function (path) {
		if($location.path().substr(0, path.length) == path) {
			return "active";
		} else {
			return "";
		}
	};

	$scope.getClass = function (paths) {
		for(var i = 0; i < paths.length; ++i) {
			var path = paths[i];
			if($location.path().substr(0, path.length) == path) {
				return "active";
			}
		}
		return "";
	};
}]);
