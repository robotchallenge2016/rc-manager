app.controller("loginFormCtrl", ["$scope", "$timeout", "$location", "$window", "authnService", "alertService",
	function ($scope, $timeout, $location, $window, authnService, alertService) {

	$scope.login = function (credentials) {
		$scope.inProgress = true;
		authnService.login(credentials)
		.then(loginSuccess)
		.catch(loginFailed);
	};

	function loginSuccess() {
		alertService.success("Logowanie powiodło się!");
		$timeout(function() {
			var encodedTargetUrl = $location.search().TARGET_URL;
			if(encodedTargetUrl) {
				var unencodedTargetUrl = atob(encodedTargetUrl);
				$window.location = unencodedTargetUrl;
			} else {
				$window.location.href = '/panel/';
			}
		}, 500);
	}

	function loginFailed(error) {
		alertService.warn("Logowanie nie powiodło się! Nieprawidłowy login lub hasło.");
		$scope.inProgress = false;
	}
}]);
