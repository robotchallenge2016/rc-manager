app.directive('elementReady', [function() {
    return {
        priority: Number.MIN_SAFE_INTEGER,
        restrict: "A",
        link: function($scope, $element, $attributes) {
            $scope.$eval($attributes.elementReady);
        }
    };
}]);
