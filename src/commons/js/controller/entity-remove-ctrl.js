app.controller("entityRemoveCtrl", ["$scope", "$uibModalInstance", "entity", function($scope, $uibModalInstance, entity) {

	$scope.entity = entity;

	$scope.ok = function() {
		$uibModalInstance.close(entity);
	};

	$scope.close = function() {
		$uibModalInstance.dismiss();
	};
}]);
