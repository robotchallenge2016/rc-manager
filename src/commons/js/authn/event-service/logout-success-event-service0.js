app.service("logoutSuccessEventService", ["$rootScope", "AUTHN_EVENTS", function ($rootScope, AUTHN_EVENTS) {
    this.broadcast = function() {
    	$rootScope.$broadcast(AUTHN_EVENTS.logoutSuccess);
    };

    this.listen = function(callback) {
    	$rootScope.$on(AUTHN_EVENTS.logoutSuccess, callback);
    };
}]);
