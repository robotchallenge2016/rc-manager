app.service("introspectionLoadedEventService", ["$rootScope", "AUTHN_EVENTS", function ($rootScope, AUTHN_EVENTS) {
    this.broadcast = function() {
    	$rootScope.$broadcast(AUTHN_EVENTS.introspectionLoaded);
    };

    this.listen = function(callback) {
    	$rootScope.$on(AUTHN_EVENTS.introspectionLoaded, callback);
    };
}]);
