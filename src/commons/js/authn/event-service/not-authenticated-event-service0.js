app.service("notAuthenticatedEventService", ["$rootScope", "AUTHN_EVENTS", function ($rootScope, AUTHN_EVENTS) {
    this.broadcast = function() {
    	$rootScope.$broadcast(AUTHN_EVENTS.notAuthenticated);
    };

    this.listen = function(callback) {
    	$rootScope.$on(AUTHN_EVENTS.notAuthenticated, callback);
    };
}]);
