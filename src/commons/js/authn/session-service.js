app.service("sessionService", ["$http", "introspectionLoadedEventService", "notAuthenticatedEventService",
	function ($http, introspectionLoadedEventService, notAuthenticatedEventService) {

	var self = this;

	this.systemUser = null;

	this.create = function (introspection) {
		this.systemUser = introspection;
	};

	this.destroy = function () {
		this.systemUser = null;
	};

	this.isIntrospected = function () {
		return this.systemUser !== null;
	};

	this.introspection = function() {
		return $http.get("/rc-webapp/v1/system/authn/introspection/")
		.then(createSession);
	};

	notAuthenticatedEventService.listen(this.destroy);

	function createSession(introspection) {
		self.create(introspection.data);
		introspectionLoadedEventService.broadcast();
	}
}]);
