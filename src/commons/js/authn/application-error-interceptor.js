app.factory('applicationErrorInterceptor', [
    "$rootScope", "$q", "alertService", "APPLICATION_ERROR_CODES",
    function ($rootScope, $q, alertService, APPLICATION_ERROR_CODES) {
    return {
        responseError: function (response) {
            if(response.status == 400 || response.status == 409 || response.status >= 500) {
                if(response.data) {
                    var msg = APPLICATION_ERROR_CODES[response.data.code.toUpperCase()];
                    if(!msg) {
                        msg = "Wystąpił problem z obsługą zapytania: " + response.data.incidentId;
                    }
                    alertService.warn(msg);
                }
            }
            return $q.reject(response);
        }
    };
}]);
