app.service("authnService", ["$http", "sessionService",
	function ($http, session) {

	this.login = function(credentials) {
		return $http.post("/rc-webapp/v1/system/authn/login/j_security_check", serializeData(credentials), {
			"headers" : {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		});
	};

	this.logout = function() {
		return $http.post("/rc-webapp/v1/system/authn/logout")
		.then(function() {
			session.destroy();
		});
	};

	function serializeData(credentials) {
		return $.param({
			"j_username" : credentials.email,
			"j_password" : credentials.password
		});
	}

	this.isAuthenticated = function() {
		return session.isIntrospected();
	};
}]);
