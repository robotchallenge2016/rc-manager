app.constant("AUTHN_EVENTS", {
  loginSuccess: "login-success",
  loginFailed: "login-failed",
  logoutSuccess: "logout-success",
  notAuthenticated: "not-authenticated",
  introspectionLoaded: "introspection-loaded"
});
