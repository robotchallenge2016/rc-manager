app.constant("APPLICATION_ERROR_CODES", {
    // General error codes
    ENTITY_NOT_FOUND: "Wybrany element nie został znaleziony !",
    STALE_OBJECT: "Wysłane dane są nieaktualne. Odśwież stronę i spróbuj ponownie",

    // Registration and verification REST API error codes
    REGISTRATION_CLOSED: "Konto nie zostało utworzone. Rejestracja jest zamknięta !",

    // SystemUser REST API error codes
    STILL_CONNECTED_TO_ROBOTS: "Użytkownik nadal posiada przypisane roboty",

    // Competitor REST API error codes
    TOO_MANY_ROBOTS_OF_TYPE_PER_COMPETITOR: "Zarejestrowano maksymalną ilość robotów wybranego typu !",
    ALREADY_PROMOTED: "Użytkownik już został promowany do rangi USER",

    // Robot REST API error codes
    ROBOT_NAME_ALREADY_EXISTS: "Nazwa robota już istnieje !",
    ROBOT_CONNECTED_TO_RESULT_OR_MATCH_SCHEDULE: "Robot jest powiązany z wynikiem lub terminarzem meczów !",
    ROBOT_ASSIGNED_TO_COMPETITION: "Robot jest powiązany z konkrencją !",

    // Reception REST API error codes
    CANT_ACCEPT_TOURNAMENT_ALREADY_STARTED: "Nie można przyjmować uczestników. Turniej został rozpoczęty !",

    // Competition REST API error codes
    CANT_START_TOURNAMENT_NOT_OPENED: "Nie można rozpocząć konkurencji jeśli turniej nie jest rozpoczęty",

    // Competition result REST API error codes
    COMPETITION_NOT_STARTED: "Nie można tworzyć/aktualizować wyników konkurencji, która nie została rozpoczęta !",
    COMPETITION_FINISHED: "Nie można tworzyć/aktualizować wyników konkurencji, która została zakończona !",

    // SumoCompetition result REST API error codes
    UNABLE_TO_GENERATE_SCHEDULE: "Nie można utworzyć terminarza dla konkurencji, która został rozpoczęta lub zakończona !",

    // LineFollower result
    TOO_MANY_RESULTS_PER_ROBOT_IN_COMPETITION: "Robot wykorzystał maksymalną ilość przejazdów"
});
