app.factory('authnInterceptor', [
    "$rootScope", "$q", "alertService", "AUTHN_EVENTS",
    function ($rootScope, $q, alertService, AUTHN_EVENTS) {
        return {
            responseError: function (response) {
                if(response.status == 401) {
                    alertService.warn("Użytkownik nie jest uwierzytelniony. Przekierowanie do strony logowania.");
                }
                $rootScope.$broadcast({
                    401: AUTHN_EVENTS.notAuthenticated
                }[response.status], response);

                return $q.reject(response);
            }
        };
}]);
