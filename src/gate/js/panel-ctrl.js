app.controller("panelController",
    ["$scope", "$q", "$timeout", "$window", "dictionaryService", "alertService",
 	function ($scope, $q, $timeout, $window, dictionaryService, alertService) {
    // Preloader
    $scope.isInitialized = false;

	$scope.alerts = alertService.alerts();
	$scope.closeAlert = alertService.closeAlert;

    dictionaryService.retrieveDictionaries();

    $scope.appReady = function() {
        $scope.isInitialized = true;
    };

    $scope.goBack = function() {
        window.history.back();
    };
}]);
