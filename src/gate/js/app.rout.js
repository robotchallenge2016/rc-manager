app.config(["$routeProvider", function ($routeProvider) {
	$routeProvider

	.when("/", {
		redirectTo: "/tournament/result/linefollower"
	})

	// Not found page
	.otherwise({
		templateUrl: "views/core/navigation/not-found.view.html"
	});
}]);
