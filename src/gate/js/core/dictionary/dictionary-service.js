app.service("dictionaryService", ["$http", "$q", "dictionaryLoadedEventService", "alertService",
    function($http, $q, dictionaryLoadedEventService, alertService) {

    var self = this;
	this.systemRoles = [];
    this.competitionTypes = [];
    this.competitionSubtypes = [];
    this.robotTypes = [];

    this.LINEFOLLOWER_COMPETITION = "LINEFOLLOWER";
    this.SUMO_COMPETITION = "SUMO";
    this.FREESTYLE_COMPETITION = "FREESTYLE";

    this.retrieveDictionaries = function() {
        retrieveCompetitionTypes();
        retrieveRobotTypes();
        retrieveCompetitionSubtypes();

        return $q.all([
    		retrieveCompetitionTypes(),
            retrieveRobotTypes(),
            retrieveCompetitionSubtypes()
    	]).then(function(result) {
    		dictionaryLoadedEventService.broadcast();
    	});
    };

    this.competitionTypeByGuid = function(guid) {
    	return this.competitionTypes.filter(function(competitionType) {
    		return competitionType.guid == guid;
    	})[0];
    };

    function retrieveCompetitionTypes() {
 		return $http.get("https://localhost:3000/rc-webapp/v1/tournament/dictionary/competition/type/all",
 		{
 			cache : true
 		})
		.then(function (result) {
			self.competitionTypes = result.data;
		});
    }

    this.competitionSubtypeByCompetitionType = function(competitionType) {
        return this.competitionSubtypes.filter(function(competitionSubtype) {
            return competitionSubtype.competitionType.name == competitionType;
        });
    };

    this.competitionSubtypeByRobotType = function(robotType) {
        return this.competitionSubtypes.filter(function(competitionSubtype) {
            return competitionSubtype.robotType.guid == robotType.guid;
        })[0];
    };

    this.competitionSubtypeByGuid = function(guid) {
    	return this.competitionSubtypes.filter(function(competitionSubtype) {
    		return competitionSubtype.guid == guid;
    	})[0];
    };

    function retrieveCompetitionSubtypes() {
 		return $http.get("https://localhost:3000/rc-webapp/v1/tournament/dictionary/competition/subtype/all",
 		{
 			cache : true
 		})
		.then(function (result) {
			self.competitionSubtypes = result.data;
		});
    }

    this.robotTypeByGuid = function(guid) {
    	return this.robotTypes.filter(function(robotType) {
    		return robotType.guid == guid;
    	})[0];
    };

    function retrieveRobotTypes() {
 		return $http.get("https://localhost:3000/rc-webapp/v1/tournament/dictionary/robot/type/all",
 		{
 			cache : true
 		})
		.then(function (result) {
			self.robotTypes = result.data;
		});
    }
}]);
