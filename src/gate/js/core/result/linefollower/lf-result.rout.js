app.config(["$routeProvider", "LF_RESULT_API_CONSTANTS", function ($routeProvider, LF_RESULT_API_CONSTANTS) {
	$routeProvider

    // Linefollower Result
	.when(LF_RESULT_API_CONSTANTS.lfCompetitionList, {
		templateUrl: "views/core/result/linefollower/lf-competition-result-list.view.html",
		controller: "lfCompetitionListCtrl"
	})
	.when(LF_RESULT_API_CONSTANTS.gate, {
		templateUrl: "views/core/result/linefollower/lf-result-gate-start.view.html",
		controller: "startLfGateCtrl"
	});
}]);
