app.controller("lfCompetitionListCtrl",
    ["$scope", "$uibModal", "lfCompetitionService", "lfResultApi", "alertService",
    function ($scope, $uibModal, competitionService, lfResultApi, alertService) {

	$scope.competitions = [];
    $scope.lfResultApi = lfResultApi;

	$scope.getAll = function() {
		competitionService.getAll()
		.then(function(result) {
			$scope.competitions = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista konkurencji nie została pobrana !");
		});
	};

    $scope.getAll();
}]);
