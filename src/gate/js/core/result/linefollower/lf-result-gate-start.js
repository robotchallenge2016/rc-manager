app.controller("startLfGateCtrl",
	["$scope", "$routeParams", "$q", "alertService", "lfResultService", "lfResultApi", "robotService", "lfCompetitionService",
	function ($scope, $routeParams, $q, alertService, lfResultService, lfResultApi, robotService, lfCompetitionService) {

	$scope.startInfo = {
		ip: "192.168.1.108"
	};
	$scope.competition = {
		guid: $routeParams.competitionGuid
	};
	$scope.lfResultApi = lfResultApi;

	$scope.$watch("startInfo.selectedCompetition", function(newValue, oldValue) {
		if(newValue) {
			removeSelectedRobot();
		}
	});

	$scope.save = function(startInfo) {
		var data = angular.copy(startInfo);
		data.competitionGuid = startInfo.selectedCompetition.guid;
		data.robotGuid = startInfo.selectedRobot.guid;

		delete data.ip;
		delete data.selectedRobot;
		delete data.selectedCompetition;

		lfResultService.startGate(startInfo.ip, data)
		.then(function(result) {
			alertService.success("Bramka przyjęła żadanie !");
		});
	};

	$q.all([
		lfCompetitionService.getAll()
	]).then(function(result) {
		$scope.competitions = result[0].data;
		if($scope.competitions.length > 0) {
			$scope.startInfo.selectedCompetition = $scope.competitions[0];
		}
	});

	$scope.searchRobots = function(search) {
		if($scope.startInfo.selectedCompetition) {
			lfCompetitionService.searchRobotInCompetitionMatchingAnyParam($scope.startInfo.selectedCompetition.guid, search)
			.then(function(result) {
				if(result !== null) {
					$scope.availableRobots = result.data;
				}
			});
		}
	};

	function removeSelectedRobot() {
		$scope.startInfo.selectedRobot = {};
		$scope.availableRobots = [];
	}
}]);
