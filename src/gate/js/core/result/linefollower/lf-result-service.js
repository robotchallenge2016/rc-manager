app.service("lfResultService", ["$http", function ($http) {

	this.startGate = function(ip, details)
	{
		return $http.post("https://" + ip + "/gate/start", details);
	}
}]);
