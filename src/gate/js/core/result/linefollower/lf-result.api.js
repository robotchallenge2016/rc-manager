app.constant("LF_RESULT_API_CONSTANTS", {
    lfCompetitionList: "/tournament/result/linefollower",
    gate: "/tournament/result/linefollower/competition/:competitionGuid/gate/start"
});

app.service("lfResultApi", ["$location", "LF_RESULT_API_CONSTANTS", function($location, LF_RESULT_API_CONSTANTS) {

    this.goToLfCompetitionList = function() {
        $location.path(LF_RESULT_API_CONSTANTS.lfCompetitionList);
    };

    this.goToGate = function(competitionGuid) {
        $location.path(LF_RESULT_API_CONSTANTS.gate.replace(":competitionGuid", competitionGuid));
    };
}]);
