app.service("alertService", function() {
	var alerts = [];

    this.success = function(content) {
      alerts.push({type: "success", msg: content});
    };

    this.info = function(content) {
    	alerts.push({type: "info", msg: content});
  	};

    this.warn = function(content) {
      alerts.push({type: "warning", msg: content});
    };

    this.error = function(content) {
      alerts.push({type: "danger", msg: content});
    };

  	this.closeAlert = function(index) {
    	alerts.splice(index, 1);
  	};

  	this.alerts = function() {
  		return alerts;
  	};
});
