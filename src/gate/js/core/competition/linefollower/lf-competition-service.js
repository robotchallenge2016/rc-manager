app.service("lfCompetitionService", ["$http", "$q", "alertService", function ($http, $q, alertService) {

	this.create = function(competition) {
		return $http.post("/rc-webapp/v1/tournament/competition/linefollower", competition);
	};

	this.update = function(competition) {
		return $http.put("/rc-webapp/v1/tournament/competition/linefollower/guid/" + competition.guid, competition);
	};

	this.delete = function(guid) {
		return $http.delete("/rc-webapp/v1/tournament/competition/linefollower/guid/" + guid);
	};

	this.getAll = function() {
		return $http.get("/rc-webapp/v1/tournament/competition/linefollower/all");
	};

	this.getByCompetitorGuid = function(guid) {
		return $http.get("/rc-webapp/v1/tournament/competition/linefollower/competitor/guid/" + guid);
	};

	this.searchRobotInCompetitionMatchingAnyParam = function(guid, search) {
		if(search.length > 2) {
			var params = {search: search};
			return $http.get("/rc-webapp/v1/tournament/competition/linefollower/guid/" + guid + "/robot/match/any", {params: params});
		}
		return $q.when(null);
	};

	this.getByGuid = function(guid) {
		return $http.get("/rc-webapp/v1/tournament/competition/linefollower/guid/" + guid);
	};

	this.nameIsFree = function(competitionName) {
		if(competitionName !== null) {
			return $http.get("/rc-webapp/v1/tournament/competition/linefollower/name/" + competitionName + "/free");
		}
		return $q.when(true);
	};

	this.startCompetition = function(competition) {
		return $http.post("/rc-webapp/v1/tournament/competition/linefollower/start/guid/" + competition.guid , competition);
	};

	this.stopCompetition = function(competition) {
		return $http.post("/rc-webapp/v1/tournament/competition/linefollower/stop/guid/" + competition.guid, competition);
	};
}]);
