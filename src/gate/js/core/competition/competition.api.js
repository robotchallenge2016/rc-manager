app.constant("COMPETITION_API_CONSTANTS", {
    list: "/tournament/competition",
    details: "/tournament/competition/:competitionType"
});

app.service("competitionApi", ["$location", "COMPETITION_API_CONSTANTS", function($location, COMPETITION_API_CONSTANTS) {
    this.goToList = function() {
        $location.path(COMPETITION_API_CONSTANTS.list);
    };

    this.goToDetails = function(competitionType) {
        $location.path(COMPETITION_API_CONSTANTS.details.replace(":competitionType", competitionType.toLowerCase()));
    };
}]);
