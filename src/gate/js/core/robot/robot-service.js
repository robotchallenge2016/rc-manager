app.service("robotService", ["$http", "$q", function ($http, $q) {

	this.searchMatchingAnyParam = function(search) {
		if(search.length > 2) {
			var params = {search: search};
			return $http.get("/rc-webapp/v1/tournament/robot/match/any", {params: params});
		}
		return $q.when(null);
	};
}]);
