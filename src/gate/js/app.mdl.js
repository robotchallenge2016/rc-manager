var app = angular.module("app", [
    "ngRoute", "ngAnimate", "ngSanitize", "angular-loading-bar",
    "ui.bootstrap", "ui.select", "ui.validate"]);

app.config(["uiSelectConfig", function(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
}]);
