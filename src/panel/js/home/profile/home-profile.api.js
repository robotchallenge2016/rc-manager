app.constant("HOME_PROFILE_API_CONSTANTS", {
    preview: "/home/profile"
});

app.service("homeProfileApi", ["$location", "HOME_PROFILE_API_CONSTANTS", function($location, HOME_PROFILE_API_CONSTANTS) {

    this.goToPreview = function() {
        $location.path(HOME_PROFILE_API_CONSTANTS.preview);
    };
}]);
