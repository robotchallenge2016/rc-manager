app.config(["$routeProvider", "HOME_PROFILE_API_CONSTANTS", function ($routeProvider, HOME_PROFILE_API_CONSTANTS) {
	$routeProvider

    .when(HOME_PROFILE_API_CONSTANTS.preview, {
		templateUrl: "views/home/profile/home-profile.view.html",
		controller: "homeProfileCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	});
}]);
