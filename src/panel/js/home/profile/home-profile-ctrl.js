app.controller("homeProfileCtrl",
	["$scope", "$q", "sessionService", "competitorService", "robotService", "homeRobotApi", "alertService",
	function ($scope, $q, sessionService, competitorService, robotService, homeRobotApi, alertService) {

	var competitor = {};
	competitor.guid = sessionService.systemUser.guid;

	$scope.competitor = competitor;
	$scope.memento = {};

	// loader icons
	$scope.saveInProgress = false;

	$scope.save = function(competitor) {
		saveInProgress(true);

		var data = angular.copy(competitor);
		delete data.robots;
		delete data.confirmPassword;

		competitorService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			homeRobotApi.goToList();
			saveInProgress(false);
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	competitorService.getByGuid(competitor.guid)
		.then(function(result) {
			var competitor = result.data;
			competitor.robots = $scope.competitor.robots;
			$scope.competitor = competitor;
			$scope.memento.competitor = angular.copy($scope.competitor);
		});

	robotService.getByCompetitorGuid(competitor.guid)
		.then(function(result) {
			$scope.competitor.robots = result.data;
		});

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
