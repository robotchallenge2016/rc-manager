app.controller("homeCompetitionListCtrl",
	["$scope", "competitionService", "homeCompetitionApi", "dictionaryLoadedEventService", "alertService",
	function ($scope, competitionService, homeCompetitionApi, dictionaryLoadedEventService, alertService) {

	$scope.competitions = [];
	$scope.homeCompetitionApi = homeCompetitionApi;

	dictionaryLoadedEventService.listen(initialize);

	initialize();

	function initialize() {
		if($scope.systemUser) {
			competitionService.getByCompetitorGuid($scope.systemUser.guid)
			.then(function(result) {
				$scope.competitions = result.data;
			})
			.catch(function(error) {
				alertService.warn("Nie udało się pobrać listy konkurencji!");
			});
		}
	}
}]);
