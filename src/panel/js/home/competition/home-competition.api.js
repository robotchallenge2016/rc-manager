app.constant("HOME_COMPETITION_API_CONSTANTS", {
    list: "/home/tournament/competition",
    preview: "/home/tournament/competition/preview/:competitionGuid"
});

app.service("homeCompetitionApi", ["$location", "HOME_COMPETITION_API_CONSTANTS", function($location, HOME_COMPETITION_API_CONSTANTS) {

    this.goToList = function() {
        $location.path(HOME_COMPETITION_API_CONSTANTS.list);
    };

    this.goToPreview = function(competitionGuid) {
        $location.path(HOME_COMPETITION_API_CONSTANTS.preview.replace(":competitionGuid", competitionGuid));
    };
}]);
