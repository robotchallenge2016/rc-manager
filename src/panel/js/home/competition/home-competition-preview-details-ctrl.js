app.controller("homeCompetitionPreviewDetailsCtrl",
	["$scope", "$routeParams", "$q", "competitionService", "homeCompetitionApi", "robotService", "homeRobotApi",
		"dictionaryService", "alertService", "dictionaryLoadedEventService",
	function ($scope, $routeParams, $q, competitionService, homeCompetitionApi, robotService, homeRobotApi,
				dictionaryService, alertService, dictionaryLoadedEventService) {

	dictionaryLoadedEventService.listen(initialize);

	$scope.competition = {
		guid: $routeParams.competitionGuid
	};
	$scope.robots = [];
	$scope.homeCompetitionApi = homeCompetitionApi;
	$scope.homeRobotApi = homeRobotApi;

	initialize();

	function initialize() {
		$scope.competitionSubtypes = dictionaryService.competitionSubtypes;
	}

	competitionService.getByGuid($scope.competition.guid)
	.then(function(result) {
		$scope.competition = result.data;
		$scope.competition.competitionSubtype = dictionaryService.competitionSubtypeByGuid($scope.competition.competitionSubtypeGuid);
		if(result.data.robotGuids) {
			robotService.getByGuids(result.data.robotGuids)
			.then(function(result) {
				$scope.robots = result.data;
			})
			.catch(function(error) {
				alertService.warn("Lista robotów nie została pobrana !");
			});
		}
	})
	.catch(function(error) {
		alertService.warn("Nie udało się pobrać szczegółów konkurencji !");
	});
}]);
