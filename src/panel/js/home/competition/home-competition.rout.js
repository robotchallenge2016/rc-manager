app.config(["$routeProvider", "HOME_COMPETITION_API_CONSTANTS", function ($routeProvider, HOME_COMPETITION_API_CONSTANTS) {
	$routeProvider

    .when(HOME_COMPETITION_API_CONSTANTS.list, {
		templateUrl: "views/home/competition/home-competition-list.view.html",
		controller: "homeCompetitionListCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_COMPETITION_API_CONSTANTS.preview, {
		templateUrl: "views/home/competition/home-competition-preview.view.html",
		controller: "homeCompetitionPreviewDetailsCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	});
}]);
