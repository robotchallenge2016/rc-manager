app.controller("homeLineFollowerRobotResultListCtrl",
    ["$scope", "$routeParams", "lfResultService", "homeRobotApi", "homeResultApi", "alertService",
    function ($scope, $routeParams, lfResultService, homeRobotApi, homeResultApi, alertService) {

    $scope.robot = {
        guid: $routeParams.robotGuid
    };
	$scope.linefollowerResults = [];
    $scope.homeRobotApi = homeRobotApi;
    $scope.homeResultApi = homeResultApi;

	$scope.getAll = function() {
		lfResultService.getByRobot($scope.robot.guid)
		.then(function(result) {
			$scope.linefollowerResults = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista wyników nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
