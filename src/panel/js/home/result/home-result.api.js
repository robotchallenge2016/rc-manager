app.constant("HOME_RESULT_API_CONSTANTS", {
    lfResultList: "/home/tournament/robot/result/linefollower/:robotGuid",
    sumoMatchList: "/home/tournament/robot/result/sumo/:robotGuid",
    sumoMatchPreview: "/home/tournament/robot/:robotGuid/result/sumo/preview/:sumoMatchGuid",
    fsResultList: "/home/tournament/robot/result/freestyle/:robotGuid"
});

app.service("homeResultApi", ["$location", "HOME_RESULT_API_CONSTANTS", function($location, HOME_RESULT_API_CONSTANTS) {

    this.goToLfResultList = function(robotGuid) {
        $location.path(HOME_RESULT_API_CONSTANTS.lfResultList.replace(":robotGuid", robotGuid));
    };

    this.goToSumoMatchList = function(robotGuid) {
        $location.path(HOME_RESULT_API_CONSTANTS.sumoMatchList.replace(":robotGuid", robotGuid));
    };

    this.goToSumoMatchPreview = function(robotGuid, sumoMatchGuid) {
        $location.path(HOME_RESULT_API_CONSTANTS.sumoMatchPreview.replace(":robotGuid", robotGuid)
            .replace(":sumoMatchGuid", sumoMatchGuid));
    };

    this.goToFsResultList = function(robotGuid) {
        $location.path(HOME_RESULT_API_CONSTANTS.fsResultList.replace(":robotGuid", robotGuid));
    };
}]);
