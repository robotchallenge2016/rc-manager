app.controller("homeSumoRobotResultListCtrl",
    ["$scope", "$routeParams", "sumoMatchService", "homeRobotApi", "homeResultApi", "alertService",
    function ($scope, $routeParams, sumoMatchService, homeRobotApi, homeResultApi, alertService) {

    $scope.robot = {
        guid: $routeParams.robotGuid
    };
	$scope.sumoMatches = [];
    $scope.homeResultApi = homeResultApi;
    $scope.homeRobotApi = homeRobotApi;

	$scope.getAll = function() {
		sumoMatchService.getByRobot($scope.robot.guid)
		.then(function(result) {
			$scope.sumoMatches = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista wyników nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
