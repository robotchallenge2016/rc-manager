app.controller("previewSumoMatchCtrl",
    ["$scope", "$location", "$routeParams", "sumoMatchService", "homeResultApi", "alertService",
    function ($scope, $location, $routeParams, sumoMatchService, homeResultApi, alertService) {

    $scope.robot = {
        guid: $routeParams.robotGuid
    };
    $scope.sumoMatch = {
        guid: $routeParams.sumoMatchGuid
    };
    $scope.memento = {};
    $scope.newResult = {};
    $scope.homeResultApi = homeResultApi;

    $scope.$watch("newResult.winner", function(newValue, oldValue) {
		if(newValue) {
            if($scope.robots[0].guid === newValue.guid) {
                $scope.newResult.looser = $scope.robots[1];
            } else {
                $scope.newResult.looser = $scope.robots[0];
            }
		}
	});

    sumoMatchService.getByGuid($scope.sumoMatch.guid)
    .then(function(result) {
		$scope.sumoMatch = result.data;
		$scope.memento.sumoMatch = angular.copy($scope.sumoMatch);

        $scope.sumoMatch.detailedResults = [];
        for(var i = 0; i < $scope.sumoMatch.results.length; ++i) {
            var detailedResult = {
                guid: guid()
            };
            if($scope.sumoMatch.results[i].winnerGuid === $scope.sumoMatch.home.guid) {
                detailedResult.winner = $scope.sumoMatch.home;
                detailedResult.looser = $scope.sumoMatch.away;
            } else {
                detailedResult.winner = $scope.sumoMatch.away;
                detailedResult.looser = $scope.sumoMatch.home;
            }
            $scope.sumoMatch.detailedResults.push(detailedResult);
        }
        $scope.robots = [ $scope.sumoMatch.home, $scope.sumoMatch.away ];
	})
	.catch(function(error) {
		alertService.warn("Nie udało się pobrać szczegółów meczu Sumo !");
	});

    $scope.addResult = function() {
        if(!$scope.sumoMatch.detailedResults) {
            $scope.sumoMatch.detailedResults = [];
        }
        var result = {
            guid: guid(),
            winner: $scope.newResult.winner,
            looser: $scope.newResult.looser
        };
        $scope.sumoMatch.detailedResults.push(result);

        $scope.newResult = {};
    };

    $scope.removeResult = function(result) {
        $scope.sumoMatch.detailedResults = $scope.sumoMatch.detailedResults.filter(function (r) {
		  return r.guid !== result.guid;
		});
		$scope.sumoMatchForm.$setDirty();
    };

    $scope.save = function(sumoMatch) {
		var data = angular.copy(sumoMatch);

        data.results = [];
        for(var i = 0; i < data.detailedResults.length; ++i) {
            result = {
                winnerGuid: data.detailedResults[i].winner.guid,
                looserGuid: data.detailedResults[i].looser.guid
            };
            data.results.push(result);
        }

        delete data.detailedResults;
		delete data.home;
		delete data.away;

		sumoMatchService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			$location.path("/tournament/result/sumo");
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
		});
	};

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }
}]);
