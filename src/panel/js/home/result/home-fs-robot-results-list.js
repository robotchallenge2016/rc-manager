app.controller("homeFreestyleRobotResultListCtrl",
    ["$scope", "$routeParams", "fsResultService", "homeRobotApi", "homeResultApi", "alertService",
    function ($scope, $routeParams, fsResultService, homeRobotApi, homeResultApi, alertService) {

    $scope.robot = {
        guid: $routeParams.robotGuid
    };
	$scope.freestyleResults = [];
    $scope.homeRobotApi = homeRobotApi;
    $scope.homeResultApi = homeResultApi;

	$scope.getAll = function() {
		fsResultService.getByRobot($scope.robot.guid)
		.then(function(result) {
			$scope.freestyleResults = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista wyników nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
