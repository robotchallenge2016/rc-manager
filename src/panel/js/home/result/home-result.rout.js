app.config(["$routeProvider", "HOME_RESULT_API_CONSTANTS", function ($routeProvider, HOME_RESULT_API_CONSTANTS) {
	$routeProvider

    .when(HOME_RESULT_API_CONSTANTS.lfResultList, {
		templateUrl: "views/home/result/linefollower/home-lf-result-list.view.html",
		controller: "homeLineFollowerRobotResultListCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_RESULT_API_CONSTANTS.sumoMatchList, {
		templateUrl: "views/home/result/sumo/home-sumo-match-list.view.html",
		controller: "homeSumoRobotResultListCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_RESULT_API_CONSTANTS.sumoMatchPreview, {
		templateUrl: "views/home/result/sumo/home-sumo-match-preview.view.html",
		controller: "previewSumoMatchCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_RESULT_API_CONSTANTS.fsResultList, {
		templateUrl: "views/home/result/freestyle/home-fs-result-list.view.html",
		controller: "homeFreestyleRobotResultListCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	});
}]);
