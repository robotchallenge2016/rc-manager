app.config(["$routeProvider", "HOME_TEAM_API_CONSTANTS", function ($routeProvider, HOME_TEAM_API_CONSTANTS) {
	$routeProvider

	.when(HOME_TEAM_API_CONSTANTS.create, {
		templateUrl: "views/home/team/home-team-create-details.view.html",
		controller: "homeCreateTeamDetailsCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_TEAM_API_CONSTANTS.update, {
		templateUrl: "views/home/team/home-team-update-details.view.html",
		controller: "homeUpdateTeamDetailsCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	});
}]);
