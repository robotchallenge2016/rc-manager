app.constant("HOME_TEAM_API_CONSTANTS", {
    create: "/home/tournament/team/create",
    update: "/home/tournament/team/details"
});

app.service("homeTeamApi", ["$location", "HOME_TEAM_API_CONSTANTS", function($location, HOME_TEAM_API_CONSTANTS) {

    this.goToCreate = function() {
        $location.path(HOME_TEAM_API_CONSTANTS.create);
    };

    this.goToUpdate = function(teamGuid) {
        $location.path(HOME_TEAM_API_CONSTANTS.update.replace(":teamGuid", teamGuid));
    };
}]);
