app.controller("homeCreateTeamDetailsCtrl",
	["$scope", "alertService", "teamService", "homeTeamApi", "homeRobotApi", "competitorService",
	function ($scope, alertService, teamService, homeTeamApi, homeRobotApi, competitorService) {

	$scope.homeTeamApi = homeTeamApi;
	$scope.team = {
		captain: $scope.systemUser,
		competitors: []
	};
	$scope.team.captain.fullName = $scope.team.captain.firstName + " " + $scope.team.captain.lastName;

	// loader icons
	$scope.saveInProgress = false;

	$scope.save = function(team) {
		saveInProgress(true);

		var data = angular.copy(team);
		data.captainGuid = data.captain.guid;
		data.competitorGuids = [];
		for (var i = 0; i < team.competitors.length; i++) {
			data.competitorGuids[i] = team.competitors[i].guid;
		}
		delete data.captain;
		delete data.competitors;

		teamService.create(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			homeRobotApi.goToList();
			saveInProgress(false);
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	$scope.teamNameIsFree = function(teamName) {
		return teamService.nameIsFree(teamName);
	};

	$scope.refreshCompetitors = function (search) {
	    return competitorService.searchEmptyTeamMatchingAnyParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableCompetitors = response.data;
			  }
	      });
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
