app.controller("homeUpdateTeamDetailsCtrl",
	["$scope", "$routeParams", "$q", "$uibModal", "teamService", "homeTeamApi", "homeRobotApi", "competitorService", "alertService",
	function ($scope, $routeParams, $q, $uibModal, teamService, homeTeamApi, homeRobotApi, competitorService, alertService) {

	$scope.homeTeamApi = homeTeamApi;
	$scope.team = {
		competitiors: []
	};
	$scope.isCaptain = false;
	$scope.memento = {};

	// loader icons
	$scope.saveInProgress = false;

	$scope.save = function(team) {
		saveInProgress(true);

		var data = angular.copy(team);
		data.competitorGuids = [];
		for (var i = 0; i < team.competitors.length; i++) {
			data.competitorGuids[i] = team.competitors[i].guid;
		}
		delete data.competitors;

		teamService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			homeRobotApi.goToList();
			saveInProgress(false);
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	$scope.teamNameIsFree = function(teamName) {
		if(teamName != $scope.memento.team.name) {
			return teamService.nameIsFree(teamName);
		}
		return $q.when(true);
	};

	teamService.getByCompetitorGuid($scope.systemUser.guid)
	.then(function(result) {
		$scope.team = result.data;
		$scope.isCaptain = $scope.team.captainGuid == $scope.systemUser.guid;
		$scope.memento.team = angular.copy($scope.team);
		competitorService.searchTeamMembers($scope.team.guid)
		.then(function(result) {
			$scope.team.competitors = result.data;
			for(var i = 0; i < $scope.team.competitors.length; ++i) {
				var competitor = $scope.team.competitors[i];
				if(competitor.captainOfTeam) {
					$scope.captain = competitor;
					$scope.captain.fullName = competitor.firstName + " " + competitor.lastName;
					$scope.team.competitors.splice(i, 1);
					break;
				}
			}
		})
		.catch(function(err) {
			alertService.warn("Nie udało się pobrać członków zespołu");
			homeRobotApi.goToList();
		});
	})
	.catch(function(err) {
		if(err.status == 404) {
			homeTeamApi.goToCreate();
		}
	});

	$scope.refreshCompetitors = function (search) {
	    return competitorService.searchEmptyTeamMatchingAnyParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableCompetitors = response.data;
			  }
	      });
	};

	$scope.open = function () {

		var removeModal = $uibModal.open({
	      templateUrl: "views/home/team/home-team-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return $scope.team;
	      	}
	      }
	    });

		removeModal.result
		.then(function (team) {
			$scope.delete(team.guid);
	    });
	};

	$scope.delete = function(guid) {
		teamService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			homeRobotApi.goToList();
		})
		.catch(function(error) {
			alertService.warn("Zespół nie został usunięty !");
		});
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
