app.constant("HOME_ROBOT_API_CONSTANTS", {
    list: "/home/tournament/robot",
    create: "/home/tournament/robot/create",
    update: "/home/tournament/robot/details/:robotGuid",
    preview: "/home/tournament/robot/preview/:robotGuid"
});

app.service("homeRobotApi", ["$location", "HOME_ROBOT_API_CONSTANTS", function($location, HOME_ROBOT_API_CONSTANTS) {
    this.goToList = function() {
        $location.path(HOME_ROBOT_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(HOME_ROBOT_API_CONSTANTS.create);
    };

    this.goToUpdate = function(robotGuid) {
        $location.path(HOME_ROBOT_API_CONSTANTS.update.replace(":robotGuid", robotGuid));
    };

    this.goToPreview = function(robotGuid, replace) {
        $location.path(HOME_ROBOT_API_CONSTANTS.preview.replace(":robotGuid", robotGuid));
        if(replace) {
            $location.replace();
        }
    };
}]);
