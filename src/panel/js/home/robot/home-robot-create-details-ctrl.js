app.controller("homeRobotCreateDetailsCtrl",
	["$scope", "alertService", "robotService", "homeRobotApi", "sessionService", "systemFileService", "dictionaryService", "dictionaryLoadedEventService",
	function ($scope, alertService, robotService, homeRobotApi, sessionService, systemFileService, dictionaryService, dictionaryLoadedEventService) {

	dictionaryLoadedEventService.listen(initialize);

	$scope.robot = {
	};
	$scope.availableCompetitors = [];
	$scope.homeRobotApi = homeRobotApi;

	// loader icons
	$scope.saveInProgress = false;

	initialize();

	function initialize() {
		$scope.robotTypes = dictionaryService.robotTypes;
		if($scope.robotTypes.length > 0) {
			$scope.robot.robotType = $scope.robotTypes[0];
		}
	}

	$scope.save = function(robot) {
		saveInProgress(true);

		var data = angular.copy(robot);
		data.robotTypeGuid = robot.robotType.guid;
		data.ownerGuid = sessionService.systemUser.guid;
		delete data.owner;
		delete data.image;

		robotService.create(data)
		.then(function(result) {
			if(imageWasAdded()) {
				systemFileService.upload("/home/tournament/robot", result.data.guid, robot.image.file)
				.then(function(result) {
					alertService.success("Zapisano !");
					homeRobotApi.goToList();
					saveInProgress(false);
				})
				.catch(function(error) {
					alertService.warn("Zdjęcie robota nie zostało zapisane !");
					saveInProgress(false);
				});
			} else {
				alertService.success("Zapisano !");
				homeRobotApi.goToList();
				saveInProgress(false);
			}
		});
	};

	$scope.robotNameIsFree = function(robotName) {
		return robotService.nameIsFree(robotName);
	};

	function imageWasAdded() {
		if(typeof $scope.robot.image !== "undefined" && $scope.robot.image !== null) {
			if(typeof $scope.robot.image.file !== "undefined" && $scope.robot.image.file !== null) {
				return true;
			}
		}
		return false;
	}

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
