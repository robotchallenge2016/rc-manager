app.controller("homeRobotUpdateDetailsCtrl",
	["$scope", "$routeParams", "$q", "robotService", "homeRobotApi", "competitorService", "teamService", "systemFileService", "dictionaryService", "alertService",
	function ($scope, $routeParams, $q, robotService, homeRobotApi, competitorService, teamService, systemFileService, dictionaryService, alertService) {

	$scope.robot = {
		guid: $routeParams.robotGuid
	};
	$scope.memento = {};
	$scope.homeRobotApi = homeRobotApi;

	// loader icons
	$scope.saveInProgress = false;

	initialize();

	function initialize() {
		$scope.robotTypes = dictionaryService.robotTypes;
	}

	$scope.$watch("robot.image.file", function(newValue, oldValue) {
		if(typeof newValue !== "undefined" && newValue !== null) {
			$scope.robotForm.robotImage.$setDirty();
		}
	});

	$scope.save = function(robot) {
		saveInProgress(true);

		var data = angular.copy(robot);
		data.ownerGuid = data.owner.guid;
		data.robotTypeGuid = data.robotType.guid;
		delete data.owner;
		delete data.robotType;
		delete data.image;

		robotService.update(data)
		.then(function(result) {
			if(imageWasAdded()) {
				systemFileService.upload("/tournament/robot", robot.guid, robot.image.file)
				.then(function(result) {
					alertService.success("Zapisano !");
					homeRobotApi.goToList();
					saveInProgress(false);
				})
				.catch(function(error) {
					alertService.warn("Zdjęcie robota nie zostało zapisane !");
					saveInProgress(false);
				});
			} else {
				alertService.success("Zapisano !");
				homeRobotApi.goToList();
				saveInProgress(false);
			}
		});
	};

	$q.all([
		robotService.getByGuid($scope.robot.guid),
		competitorService.searchRobotOwner($scope.robot.guid),

	]).then(function(result) {
		var robot = result[0].data;
		robot.owner = result[1].data;
		checkIfCurrentUserHasRightsToEditRobot(robot);

		robot.image = $scope.robot.image;
		robot.robotType = dictionaryService.robotTypeByGuid(robot.robotTypeGuid);
		$scope.robot = robot;
		$scope.memento.robot = angular.copy($scope.robot);
	});

	$scope.robotNameIsFree = function(robotName) {
		if(robotName != $scope.memento.robot.name) {
			return robotService.nameIsFree(robotName);
		}
		return $q.when(true);
	};

	function checkIfCurrentUserHasRightsToEditRobot(robot) {
		if(robot.owner.guid !== $scope.systemUser.guid) {
			if(robot.owner.teamGuid) {
				teamService.getByGuid(robot.owner.teamGuid)
				.then(function(response) {
					var team = response.data;
					if(team.competitorGuids.indexOf($scope.systemUser.guid) == -1) {
						homeRobotApi.goToPreview(robot.guid, true);
					}
				})
				.catch(function(error) {
					homeRobotApi.goToPreview(robot.guid, true);
				});
			} else {
				homeRobotApi.goToPreview(robot.guid, true);
			}
		}
	}

	function imageWasAdded() {
		if(typeof $scope.robot.image !== "undefined" && $scope.robot.image !== null) {
			if(typeof $scope.robot.image.file !== "undefined" && $scope.robot.image.file !== null) {
				return true;
			}
		}
		return false;
	}

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
