app.config(["$routeProvider", "HOME_ROBOT_API_CONSTANTS", function ($routeProvider, HOME_ROBOT_API_CONSTANTS) {
	$routeProvider
    // Competitor page
    .when(HOME_ROBOT_API_CONSTANTS.list, {
		templateUrl: "views/home/robot/home-robot-list.view.html",
		controller: "homeRobotListCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_ROBOT_API_CONSTANTS.create, {
		templateUrl: "views/home/robot/home-robot-create-details.view.html",
		controller: "homeRobotCreateDetailsCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_ROBOT_API_CONSTANTS.preview, {
		templateUrl: "views/home/robot/home-robot-preview-details.view.html",
		controller: "homeRobotPreviewDetailsCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	})
	.when(HOME_ROBOT_API_CONSTANTS.update, {
		templateUrl: "views/home/robot/home-robot-update-details.view.html",
		controller: "homeRobotUpdateDetailsCtrl",
		authz: {
			isAuthorized: "COMPETITOR"
		}
	});
}]);
