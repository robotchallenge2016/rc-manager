app.controller("homeRobotPreviewDetailsCtrl",
	["$scope", "$routeParams", "$q", "robotService", "homeRobotApi", "competitorService", "alertService",
	function ($scope, $routeParams, $q, robotService, homeRobotApi, competitorService, alertService) {

	$scope.robot = {
		guid: $routeParams.robotGuid
	};
	$scope.homeRobotApi = homeRobotApi;
	$scope.memento = {};

	$q.all([
		robotService.getByGuid($scope.robot.guid),
		competitorService.searchRobotOwner($scope.robot.guid),

	]).then(function(result) {
		var robot = result[0].data;
		robot.owner = result[1].data;
		robot.image = $scope.robot.image;
		$scope.robot = robot;

		$scope.memento.robot = angular.copy($scope.robot);
	});
}]);
