app.controller("homeRobotListCtrl",
	["$scope", "$uibModal", "robotService", "teamService", "homeRobotApi", "homeResultApi", "dictionaryLoadedEventService", "dictionaryService", "alertService",
	function ($scope, $uibModal, robotService, teamService, homeRobotApi, homeResultApi, dictionaryLoadedEventService, dictionaryService, alertService) {

	$scope.robots = [];
	$scope.homeRobotApi = homeRobotApi;
	$scope.homeResultApi = homeResultApi;

	dictionaryLoadedEventService.listen(initialize);

	function initialize() {
		$scope.getAll();
	}

	$scope.open = function (robot) {
		var removeModal = $uibModal.open({
	      templateUrl: "views/home/robot/home-robot-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return robot;
	      	}
	      }
	    });

		removeModal.result
		.then(function (robot) {
			$scope.delete(robot.guid);
	    });
	};

	$scope.delete = function(guid) {
		robotService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Robot nie został usunięty !");
		});
	};

	$scope.openResults = function(robot) {
		var competitionSubtype = dictionaryService.competitionSubtypeByRobotType(robot.robotType);
		if(competitionSubtype.competitionType.name === dictionaryService.LINEFOLLOWER_COMPETITION) {
			homeResultApi.goToLfResultList(robot.guid);
		} else if(competitionSubtype.competitionType.name === dictionaryService.SUMO_COMPETITION) {
			homeResultApi.goToSumoMatchList(robot.guid);
		} else if(competitionSubtype.competitionType.name === dictionaryService.FREESTYLE_COMPETITION) {
			homeResultApi.goToFsResultList(robot.guid);
		}

	};

	$scope.getAll = function() {
		if($scope.systemUser) {
			teamService.getByCompetitorGuid($scope.systemUser.guid)
	        .then(function(team) {
	            $scope.team = team.data;
				robotService.getByTeamGuid($scope.team.guid)
				.then(function(result) {
					$scope.robots = result.data;
				})
				.catch(function(error) {
					alertService.warn("Nie udało się pobrać listy robotów!");
				});
	        })
			.catch(function(error) {
				robotService.getByCompetitorGuid($scope.systemUser.guid)
				.then(function(result) {
					$scope.robots = result.data;
				})
				.catch(function(error) {
					alertService.warn("Nie udało się pobrać listy robotów!");
				});
			});
		}
	};

	initialize();	
}]);
