app.config(["$routeProvider", function ($routeProvider) {
	$routeProvider

	.when("/", {
		redirectTo: "/home/"
	})

	// Home page
	.when("/home", {
		redirectTo: "/home/tournament/robot"
	})

	// AuthZ page
	.when("/forbidden", {
		templateUrl: "views/core/authz/not-authorized.view.html"
	})

	// Not found page
	.otherwise({
		templateUrl: "views/core/navigation/not-found.view.html"
	});
}]);
