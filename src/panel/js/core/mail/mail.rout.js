app.config(["$routeProvider", "MAIL_API_CONSTANTS", function ($routeProvider, MAIL_API_CONSTANTS) {
	$routeProvider

    .when(MAIL_API_CONSTANTS.send, {
        templateUrl: "views/core/mail/mail.view.html",
        controller: "mailCtrl",
        authz: {
            isAuthorized: "ADMIN"
        }
    });
}]);
