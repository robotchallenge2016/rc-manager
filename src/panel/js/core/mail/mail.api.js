app.constant("MAIL_API_CONSTANTS", {
    send: "/website/mail"
});

app.service("mailApi", ["$location", "MAIL_API_CONSTANTS", function($location, MAIL_API_CONSTANTS) {
    this.goSend = function() {
        $location.path(MAIL_API_CONSTANTS.send);
    };
}]);
