app.service("mailService", ["$http", "contextService", function ($http, contextService) {

	this.send = function(mail) {
		var url = "/mail/notification";
		return $http.post(contextService.insertTournamentEditionContext(url), mail);
	};
}]);
