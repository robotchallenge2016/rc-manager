app.controller("mailCtrl",
	["$scope", "alertService", "mailService", "competitorService",
    function ($scope, alertService, mailService, competitorService) {

	// loader icons
    $scope.defaultMail = {
        title: "ZSŁ Robot Challenge 2016"
    };
    $scope.mail = angular.copy($scope.defaultMail);
	$scope.saveInProgress = false;

	$scope.save = function(mail) {
		saveInProgress(true);

		var data = angular.copy(mail);
		data.competitorGuids = [];
        if(mail.competitors) {
            for (var i = 0; i < mail.competitors.length; i++) {
    			data.competitorGuids[i] = mail.competitors[i].guid;
    		}
        }
		delete data.competitors;

		mailService.send(data)
		.then(function(result) {
			saveInProgress(false);
            clearAfterSend();
            alertService.success("Wiadomość e-mail została wysłana !");
		})
		.catch(function(error) {
			saveInProgress(false);
            alertService.warn("Nie udało się wysłać wiadomości e-mail !");
		});
	};

    $scope.refreshCompetitors = function (search) {
	    return competitorService.searchEmptyTeamMatchingAnyParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableCompetitors = response.data;
			  }
	      });
	};

    function clearAfterSend() {
        $scope.mail = angular.copy($scope.defaultMail);
    }

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}

    $scope.tinymceOptions = {
      onChange: function(e) {
        // put logic here for keypress and cut/paste changes
      },
      inline: false,
      skin: 'lightgray',
      theme : 'modern'
    };
}]);
