app.service("authzService", ["sessionService", "dictionaryService", function (sessionService, dictionaryService) {

	this.isAuthorized = function(name) {
		var systemRole = dictionaryService.systemRoleByName(name);
		return sessionService.systemUser.systemRole.priority >= systemRole.priority;
	};
}]);
