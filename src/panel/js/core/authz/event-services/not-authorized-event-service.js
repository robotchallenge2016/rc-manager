app.service("notAuthorizedEventService", ["$rootScope", "AUTHZ_EVENTS", function ($rootScope, AUTHZ_EVENTS) {
    this.broadcast = function() {
    	$rootScope.$broadcast(AUTHZ_EVENTS.notAuthorized);
    };

    this.listen = function(callback) {
    	$rootScope.$on(AUTHZ_EVENTS.notAuthorized, callback);
    };
}]);
