app.factory('authzInterceptor', ["$rootScope", "$q", "AUTHZ_EVENTS",
  function ($rootScope, $q, AUTHZ_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        403: AUTHZ_EVENTS.notAuthorized
      }[response.status], response);
      return $q.reject(response);
    }
  };
}]);
