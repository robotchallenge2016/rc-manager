app.run(["$rootScope", "AUTHZ_EVENTS", "authnService", "authzService", "notAuthenticatedEventService", "notAuthorizedEventService",
        function ($rootScope, AUTHZ_EVENTS, authnService, authzService, notAuthenticatedEventService, notAuthorizedEventService) {
    $rootScope.$on('$routeChangeStart', function (event, next) {
        if(!next.$$route) {
            return;
        }
        if(authnService.isAuthenticated()) {
            if(next.$$route.authz) {
                var isAuthorized = next.$$route.authz.isAuthorized;
                if(isAuthorized && !authzService.isAuthorized(isAuthorized)) {
                    notAuthorizedEventService.broadcast();
                }
            }
        }
    });
}]);
