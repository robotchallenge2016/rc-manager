app.controller("createSystemUserDetailsCtrl",
	["$scope", "alertService", "systemUserService", "systemUserApi", "dictionaryService", "dictionaryLoadedEventService",
	function ($scope, alertService, systemUserService, systemUserApi, dictionaryService, dictionaryLoadedEventService) {

	dictionaryLoadedEventService.listen(initialize);

	var systemUser = {};

	$scope.systemUser = systemUser;
	$scope.systemRoles = [];
	$scope.systemUserApi = systemUserApi;

	// loader icons
	$scope.saveInProgress = false;

	initialize();

	function initialize() {
		$scope.systemRoles = dictionaryService.systemRoles;
		if($scope.systemRoles.length > 0) {
			systemUser.systemRole = $scope.systemRoles[0];
		}
	}

	$scope.systemUserLoginIsFree = function(login) {
		return systemUserService.loginIsFree(login);
	};

	$scope.save = function(systemUser) {
		saveInProgress(true);

		systemUser.systemRoleGuid = systemUser.systemRole.guid;
		delete systemUser.systemRole;

		systemUserService.create(systemUser)
		.then(function(result) {
			alertService.success("Zapisano !");
			systemUserApi.goToList();
			saveInProgress(false);
		});
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
