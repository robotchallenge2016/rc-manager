app.service("systemUserService", ["$http", "contextService", function ($http, contextService) {

	this.create = function(user) {
		var url = "/system/user";
		return $http.post(contextService.insertTournamentEditionContext(url), user);
	};

	this.update = function(user) {
		var url = "/system/user/guid/" + user.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), user);
	};

	this.delete = function(guid) {
		var url = "/system/user/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/system/user/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/system/user/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.loginIsFree = function(login) {
		if(login !== null) {
			var url = "/system/user/login/" + login + "/free";
			return $http.get(contextService.insertTournamentEditionContext(url));
		}
		return $q.when(true);
	};

	this.markAsVerified = function(systemUser) {
		var url = "/system/user/guid/" + systemUser.guid + "/verified";
		return $http.post(contextService.insertTournamentEditionContext(url));
	};

	this.markAsUnverified = function(systemUser) {
		var url = "/system/user/guid/" + systemUser.guid + "/unverified";
		return $http.post(contextService.insertTournamentEditionContext(url));
	};
}]);
