app.constant("SYSTEM_USER_API_CONSTANTS", {
    list: "/system/user",
    create: "/system/user/create",
    update: "/system/user/details/:userGuid"
});

app.service("systemUserApi", ["$location", "SYSTEM_USER_API_CONSTANTS", function($location, SYSTEM_USER_API_CONSTANTS) {
    this.goToList = function() {
        $location.path(SYSTEM_USER_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(SYSTEM_USER_API_CONSTANTS.create);
    };

    this.goToUpdate = function(userGuid) {
        $location.path(SYSTEM_USER_API_CONSTANTS.update.replace(":userGuid", userGuid));
    };
}]);
