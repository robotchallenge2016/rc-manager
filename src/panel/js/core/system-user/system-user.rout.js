app.config(["$routeProvider", "SYSTEM_USER_API_CONSTANTS", function ($routeProvider, SYSTEM_USER_API_CONSTANTS) {
	$routeProvider
    // System User page
	.when(SYSTEM_USER_API_CONSTANTS.list, {
		templateUrl: "views/core/system-user/system-user-list.view.html",
		controller: "systemUserListCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(SYSTEM_USER_API_CONSTANTS.create, {
		templateUrl: "views/core/system-user/system-user-create-details.view.html",
		controller: "createSystemUserDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(SYSTEM_USER_API_CONSTANTS.update, {
		templateUrl: "views/core/system-user/system-user-update-details.view.html",
		controller: "updateSystemUserDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
