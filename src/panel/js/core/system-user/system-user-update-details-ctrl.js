app.controller("updateSystemUserDetailsCtrl", ["$scope", "$routeParams", "systemUserService", "systemUserApi",
 	"dictionaryService", "alertService", "dictionaryLoadedEventService",
	function ($scope, $routeParams, systemUserService, systemUserApi, dictionaryService, alertService, dictionaryLoadedEventService) {

	dictionaryLoadedEventService.listen(initialize);

    $scope.systemUser = {
        guid: $routeParams.userGuid
    };
	$scope.systemRoles = [];
    $scope.systemUserApi = systemUserApi;

	initialize();

	function initialize() {
		$scope.systemRoles = dictionaryService.systemRoles;
	}

	$scope.save = function(systemUser) {
        var data = angular.copy(systemUser);
		data.systemRoleGuid = data.systemRole.guid;
		delete data.systemRole;

		systemUserService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
            systemUserApi.goToList();
		});
	};

	systemUserService.getByGuid($scope.systemUser.guid)
	.then(function(result) {
		$scope.systemUser = result.data;
		$scope.systemUser.systemRole = dictionaryService.systemRoleByGuid($scope.systemUser.systemRoleGuid);
	});
}]);
