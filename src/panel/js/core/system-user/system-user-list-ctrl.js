app.controller("systemUserListCtrl",
	["$scope", "$location", "$uibModal", "systemUserService", "systemUserApi", "alertService",
	function ($scope, $location, $uibModal, systemUserService, systemUserApi, alertService) {

	$scope.systemUsers = [];
	$scope.systemUserApi = systemUserApi;

	$scope.open = function (systemUser) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/system-user/system-user-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return systemUser;
	      	}
	      }
	    });

		removeModal.result
		.then(function (systemUser) {
			$scope.delete(systemUser.guid);
	    });
	};

	$scope.delete = function(guid) {
		systemUserService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Użytkownik nie zostal usuniety !");
		});
	};

	$scope.getAll = function() {
		systemUserService.getAll()
		.then(function(result) {
			$scope.systemUsers = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista użytkowników nie została pobrana !");
		});
	};

	$scope.markAsVerified = function(systemUser) {
		systemUserService.markAsVerified(systemUser)
		.then(function(result) {
			alertService.success("Uzytkownik został aktywowany !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Nie udało się aktywwać użytkownika !");
		});
	};

	$scope.markAsUnverified = function(systemUser) {
		systemUserService.markAsUnverified(systemUser)
		.then(function(result) {
			alertService.success("Uzytkownik został dezaktywowany !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Nie udało się dezaktywować użytkownika !");
		});
	};

	$scope.getAll();
}]);
