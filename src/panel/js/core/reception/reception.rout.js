app.config(["$routeProvider", "COMPETITOR_RECEPTION_API_CONSTANTS", function ($routeProvider, COMPETITOR_RECEPTION_API_CONSTANTS) {
	$routeProvider

    .when(COMPETITOR_RECEPTION_API_CONSTANTS.competitorReception, {
		templateUrl: "views/core/reception/reception.view.html",
		controller: "competitorReceptionCtrl",
		authz: {
			isAuthorized: "USER"
		}
	});
}]);
