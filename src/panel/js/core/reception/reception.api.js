app.constant("COMPETITOR_RECEPTION_API_CONSTANTS", {
    competitorReception: "/tournament/reception"
});

app.service("competitorReceptionApi", ["$location", "COMPETITOR_RECEPTION_API_CONSTANTS", function($location, COMPETITOR_RECEPTION_API_CONSTANTS) {

    this.goToCompetitorReception = function() {
        $location.path(COMPETITOR_RECEPTION_API_CONSTANTS.competitorReception);
    };
}]);
