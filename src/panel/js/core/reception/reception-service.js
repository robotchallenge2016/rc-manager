app.service("receptionService", ["$http", function ($http) {

	this.competitorReception = function(reception) {
		return $http.post("/rc-webapp/v1/tournament/reception/competitor", reception);
	};
}]);
