app.controller("competitorReceptionCtrl",
	["$scope", "$route", "competitorService", "robotService", "teamService", "receptionService", "alertService",
	function ($scope, $route, competitorService, robotService, teamService, receptionService, alertService) {

	$scope.reception = {};

	$scope.$watch("reception.competitor", function(newValue, oldValue) {
		if(newValue) {
			teamService.getByCompetitorGuid($scope.reception.competitor.guid)
				.then(function(result) {
					$scope.reception.team = result.data;
					robotService.getByTeamGuid($scope.reception.team.guid)
						.then(function(result) {
							$scope.reception.robots = result.data;
						});
				})
				.catch(function(error) {
					if(error.status == 404) {
						robotService.getByCompetitorGuid($scope.reception.competitor.guid)
					        .then(function(result) {
					            $scope.reception.robots = result.data;
					        });
					}
				});
		}
	});

	$scope.save = function() {
		var data = {
			competitorGuid: $scope.reception.competitor.guid,
			robotGuids: []
		};
		if($scope.reception.team) {
			data.teamGuid = $scope.reception.team.guid;
		}
		for (var i = 0; i < $scope.reception.robots.length; i++) {
			data.robotGuids[i] = $scope.reception.robots[i].guid;
		}

		receptionService.competitorReception(data)
		.then(function(result) {
			alertService.success("Przybycie zawodnika zostało potwierdzone !");
			$route.reload();
		});
	};

    $scope.refreshCompetitors = function (search) {
        return competitorService.searchUnconfirmedCompetitorMatchingAnyParam(search)
          .then(function(response) {
              if(response !== null) {
                $scope.reception.availableCompetitors = response.data;
              }
          });
    };
}]);
