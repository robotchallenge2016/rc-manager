app.controller("lfResultListCtrl",
    ["$scope", "$routeParams", "$uibModal", "lfResultService", "lfResultApi", "alertService",
    function ($scope, $routeParams, $uibModal, lfResultService, lfResultApi, alertService) {

    $scope.competition = {
        guid: $routeParams.competitionGuid
    };
	$scope.linefollowerResults = [];
    $scope.lfResultApi = lfResultApi;

	$scope.open = function (linefollowerResult) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/result/result-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return linefollowerResult;
	      	}
	      }
	    });

		removeModal.result
		.then(function (linefollowerResult) {
			$scope.delete(linefollowerResult.guid);
	    });
	};

	$scope.delete = function(guid) {
		lfResultService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		});
	};

	$scope.getAll = function() {
		lfResultService.getByCompetition($scope.competition.guid)
		.then(function(result) {
			$scope.linefollowerResults = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista wyników nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
