app.controller("updateLfResultDetailsCtrl",
	["$scope", "$routeParams", "$q", "lfResultService", "lfResultApi", "robotService", "lfCompetitionService", "alertService",
	function ($scope, $routeParams, $q, lfResultService, lfResultApi, robotService, lfCompetitionService, alertService) {

	$scope.linefollowerResult = {
		guid: $routeParams.resultGuid
	};
	$scope.competition = {
		guid: $routeParams.competitionGuid
	};
	$scope.memento = {};
	$scope.lfResultApi = lfResultApi;

	$scope.save = function(linefollowerResult) {
		var data = angular.copy(linefollowerResult);
		data.competitionGuid = linefollowerResult.selectedCompetition.guid;
		data.robotGuid = linefollowerResult.selectedRobot.guid;

		delete data.selectedCompetition;
		delete data.selectedRobot;

		lfResultService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			lfResultApi.goToList($scope.competition.guid);
		});
	};

	$scope.searchRobots = function(search) {
		if($scope.linefollowerResult.selectedCompetition) {
			lfCompetitionService.searchRobotInCompetitionMatchingAnyParam($scope.linefollowerResult.selectedCompetition.guid, search)
			.then(function(result) {
				if(result !== null) {
					$scope.availableRobots = result.data;
				}
			});
		}
	};

	$q.all([
		lfCompetitionService.getAll()
	]).then(function(result) {
		$scope.competitions = result[0].data;
		if($scope.competitions.length > 0) {
			$scope.linefollowerResult.selectedCompetition = $scope.competitions[0];
		}
	});

	lfResultService.getByGuid($scope.linefollowerResult.guid)
	.then(function(result) {
		$scope.linefollowerResult = result.data;
		$scope.memento.linefollowerResult = angular.copy($scope.linefollowerResult);

		if(result.data.competitionGuid) {
			lfCompetitionService.getByGuid(result.data.competitionGuid)
			.then(function(result) {
				$scope.linefollowerResult.selectedCompetition = result.data;
			})
			.catch(function(error) {
				alertService.warn("Dane o konkurencji nie zostały pobrane !");
			});
		}

		if(result.data.robotGuid) {
			robotService.getByGuid(result.data.robotGuid)
			.then(function(result) {
				$scope.linefollowerResult.selectedRobot = result.data;
			})
			.catch(function(error) {
				alertService.warn("Dane o robocie nie zostały pobrane !");
			});
		}
	})
	.catch(function(error) {
		alertService.warn("Nie udało się pobrać szczegółów wyniku !");
	});
}]);
