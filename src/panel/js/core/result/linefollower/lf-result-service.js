app.service("lfResultService", ["$http", "contextService", function ($http, contextService) {

	this.create = function(result) {
		var url = "/result/linefollower";
		return $http.post(contextService.insertTournamentEditionContext(url), result);
	};

	this.update = function(result) {
		var url = "/result/linefollower/guid/" + result.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), result);
	};

	this.delete = function(guid) {
		var url = "/result/linefollower/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/result/linefollower/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/result/linefollower/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByCompetition = function(guid) {
		var url = "/result/linefollower/competition/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByRobot = function(guid) {
		var url = "/result/linefollower/robot/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};
}]);
