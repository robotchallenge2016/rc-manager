app.constant("LF_RESULT_API_CONSTANTS", {
    lfCompetitionList: "/tournament/result/linefollower",
    list: "/tournament/result/linefollower/competition/:competitionGuid",
    create: "/tournament/result/linefollower/competition/:competitionGuid/create",
    update: "/tournament/result/linefollower/competition/:competitionGuid/guid/:resultGuid"
});

app.service("lfResultApi", ["$location", "LF_RESULT_API_CONSTANTS", function($location, LF_RESULT_API_CONSTANTS) {

    this.goToLfCompetitionList = function() {
        $location.path(LF_RESULT_API_CONSTANTS.lfCompetitionList);
    };

    this.goToList = function(competitionGuid) {
        $location.path(LF_RESULT_API_CONSTANTS.list.replace(":competitionGuid", competitionGuid));
    };

    this.goToCreate = function(competitionGuid) {
        $location.path(LF_RESULT_API_CONSTANTS.create.replace(":competitionGuid", competitionGuid));
    };

    this.goToUpdate = function(competitionGuid, resultGuid) {
        $location.path(LF_RESULT_API_CONSTANTS.update.replace(":competitionGuid", competitionGuid)
                                                        .replace(":resultGuid", resultGuid));
    };
}]);
