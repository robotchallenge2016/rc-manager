app.config(["$routeProvider", "LF_RESULT_API_CONSTANTS", function ($routeProvider, LF_RESULT_API_CONSTANTS) {
	$routeProvider

    // Linefollower Result
	.when(LF_RESULT_API_CONSTANTS.lfCompetitionList, {
		templateUrl: "views/core/result/linefollower/lf-competition-result-list.view.html",
		controller: "lfCompetitionListCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(LF_RESULT_API_CONSTANTS.list, {
		templateUrl: "views/core/result/linefollower/lf-result-list.view.html",
		controller: "lfResultListCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(LF_RESULT_API_CONSTANTS.create, {
		templateUrl: "views/core/result/linefollower/lf-result-create-details.view.html",
		controller: "createLfResultDetailsCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(LF_RESULT_API_CONSTANTS.update, {
		templateUrl: "views/core/result/linefollower/lf-result-update-details.view.html",
		controller: "updateLfResultDetailsCtrl",
		authz: {
			isAuthorized: "USER"
		}
	});
}]);
