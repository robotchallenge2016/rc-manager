app.controller("createLfResultDetailsCtrl",
	["$scope", "$routeParams", "$q", "alertService", "lfResultService", "lfResultApi", "robotService", "lfCompetitionService",
	function ($scope, $routeParams, $q, alertService, lfResultService, lfResultApi, robotService, lfCompetitionService) {

	$scope.linefollowerResult = {};
	$scope.competition = {
		guid: $routeParams.competitionGuid
	};
	$scope.lfResultApi = lfResultApi;

	$scope.$watch("linefollowerResult.selectedCompetition", function(newValue, oldValue) {
		if(newValue) {
			removeSelectedRobot();
		}
	});

	$scope.save = function(linefollowerResult) {
		var data = angular.copy(linefollowerResult);
		data.competitionGuid = linefollowerResult.selectedCompetition.guid;
		data.robotGuid = linefollowerResult.selectedRobot.guid;

		delete data.selectedRobot;
		delete data.selectedCompetition;

		lfResultService.create(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			lfResultApi.goToList($scope.competition.guid);
		});
	};

	$q.all([
		lfCompetitionService.getAll()
	]).then(function(result) {
		$scope.competitions = result[0].data;
		if($scope.competitions.length > 0) {
			$scope.linefollowerResult.selectedCompetition = $scope.competitions[0];
		}
	});

	$scope.searchRobots = function(search) {
		if($scope.linefollowerResult.selectedCompetition) {
			lfCompetitionService.searchRobotInCompetitionMatchingAnyParam($scope.linefollowerResult.selectedCompetition.guid, search)
			.then(function(result) {
				if(result !== null) {
					$scope.availableRobots = result.data;
				}
			});
		}
	};

	function removeSelectedRobot() {
		$scope.linefollowerResult.selectedRobot = {};
		$scope.availableRobots = [];
	}
}]);
