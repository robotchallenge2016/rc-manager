app.controller("updateSumoMatchCtrl",
    ["$scope", "$routeParams", "sumoMatchService", "sumoMatchApi", "alertService",
    function ($scope, $routeParams, sumoMatchService, sumoMatchApi, alertService) {

    $scope.competition = {
        guid: $routeParams.competitionGuid
    };
    $scope.sumoMatch = {
        guid: $routeParams.resultGuid
    };
    $scope.memento = {};
    $scope.homeWins = 0;
    $scope.awayWins = 0;
    $scope.newResult = {};
    $scope.sumoMatchApi = sumoMatchApi;

    $scope.$watch("homeWins == sumoMatch.competition.requiredNumberOfWins " +
                    "|| awayWins == sumoMatch.competition.requiredNumberOfWins", function(newValue, oldValue) {
        $scope.requiredNumberOfWins = newValue;
        console.log(newValue);
    });

    $scope.$watch("newResult.winner", function(newValue, oldValue) {
		if(newValue) {
            if($scope.robots[0].guid === newValue.guid) {
                $scope.newResult.looser = $scope.robots[1];
            } else {
                $scope.newResult.looser = $scope.robots[0];
            }
		}
	});

    sumoMatchService.getByGuid($scope.sumoMatch.guid)
    .then(function(result) {
		$scope.sumoMatch = result.data;
		$scope.memento.sumoMatch = angular.copy($scope.sumoMatch);

        $scope.sumoMatch.detailedResults = [];
        for(var i = 0; i < $scope.sumoMatch.results.length; ++i) {
            var detailedResult = {
                guid: guid()
            };
            if($scope.sumoMatch.results[i].winnerGuid === $scope.sumoMatch.home.guid) {
                detailedResult.winner = $scope.sumoMatch.home;
                detailedResult.looser = $scope.sumoMatch.away;
                ++$scope.homeWins;
            } else {
                detailedResult.winner = $scope.sumoMatch.away;
                detailedResult.looser = $scope.sumoMatch.home;
                ++$scope.awayWins;
            }
            $scope.sumoMatch.detailedResults.push(detailedResult);
        }
        $scope.robots = [ $scope.sumoMatch.home, $scope.sumoMatch.away ];
	})
	.catch(function(error) {
		alertService.warn("Nie udało się pobrać szczegółów meczu Sumo !");
	});

    $scope.addResult = function() {
        if(!$scope.sumoMatch.detailedResults) {
            $scope.sumoMatch.detailedResults = [];
        }
        var result = {
            guid: guid(),
            winner: $scope.newResult.winner,
            looser: $scope.newResult.looser
        };
        if(result.winner.guid === $scope.sumoMatch.home.guid) {
            ++$scope.homeWins;
        } else {
            ++$scope.awayWins;
        }
        $scope.sumoMatch.detailedResults.push(result);

        $scope.newResult = {};
    };

    $scope.removeResult = function(result) {
        $scope.sumoMatch.detailedResults = $scope.sumoMatch.detailedResults.filter(function (r) {
            var match = r.guid === result.guid;
            if(match) {
                if(r.guid === $scope.sumoMatch.home.guid) {
                    --$scope.homeWins;
                } else {
                    --$scope.awayWins;
                }
            }
            return !match;
		});
		$scope.sumoMatchForm.$setDirty();
    };

    $scope.save = function(sumoMatch) {
		var data = angular.copy(sumoMatch);

        data.results = [];
        for(var i = 0; i < data.detailedResults.length; ++i) {
            result = {
                winnerGuid: data.detailedResults[i].winner.guid,
                looserGuid: data.detailedResults[i].looser.guid
            };
            data.results.push(result);
        }

        delete data.detailedResults;
		delete data.home;
		delete data.away;

		sumoMatchService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
            sumoMatchApi.goToList($scope.competition.guid);
		});
	};

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }
}]);
