app.constant("SUMO_MATCH_API_CONSTANTS", {
    sumoCompetitionList: "/tournament/result/sumo",
    list: "/tournament/result/sumo/competition/:competitionGuid",
    update: "/tournament/result/sumo/:competitionGuid/guid/:resultGuid"
});

app.service("sumoMatchApi", ["$location", "SUMO_MATCH_API_CONSTANTS", function($location, SUMO_MATCH_API_CONSTANTS) {

    this.goToSumoCompetitionList = function() {
        $location.path(SUMO_MATCH_API_CONSTANTS.sumoCompetitionList);
    };

    this.goToList = function(competitionGuid) {
        $location.path(SUMO_MATCH_API_CONSTANTS.list.replace(":competitionGuid", competitionGuid));
    };

    this.goToUpdate = function(competitionGuid, resultGuid) {
        $location.path(SUMO_MATCH_API_CONSTANTS.update.replace(":competitionGuid", competitionGuid)
                                                        .replace(":resultGuid", resultGuid));
    };
}]);
