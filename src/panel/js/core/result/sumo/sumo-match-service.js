app.service("sumoMatchService", ["$http", "contextService", function ($http, contextService) {

	this.update = function(sumoMatch) {
		var url = "/result/sumo/guid/" + sumoMatch.guid;
		return $http.post(contextService.insertTournamentEditionContext(url), sumoMatch);
	};

	this.getByGuid = function(guid) {
		var url = "/result/sumo/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByCompetitionGuid = function(guid) {
		var url = "/result/sumo/competition/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByRobot = function(guid) {
		var url = "/result/sumo/robot/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};
}]);
