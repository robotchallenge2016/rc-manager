app.config(["$routeProvider", "SUMO_MATCH_API_CONSTANTS", function ($routeProvider, SUMO_MATCH_API_CONSTANTS) {
	$routeProvider

	// Sumo Result
	.when(SUMO_MATCH_API_CONSTANTS.sumoCompetitionList, {
		templateUrl: "views/core/result/sumo/sumo-competition-result-list.view.html",
		controller: "sumoCompetitionListCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(SUMO_MATCH_API_CONSTANTS.list, {
		templateUrl: "views/core/result/sumo/sumo-match-list.view.html",
		controller: "sumoMatchListCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(SUMO_MATCH_API_CONSTANTS.update, {
		templateUrl: "views/core/result/sumo/sumo-match-update.view.html",
		controller: "updateSumoMatchCtrl",
		authz: {
			isAuthorized: "USER"
		}
	});
}]);
