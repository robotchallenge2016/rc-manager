app.controller("sumoMatchListCtrl",
    ["$scope", "$routeParams", "sumoMatchService", "sumoMatchApi", "robotApi", "alertService",
    function ($scope, $routeParams, sumoMatchService, sumoMatchApi, robotApi, alertService) {

    $scope.competition = {
        guid: $routeParams.competitionGuid
    };
	$scope.sumoMatches = [];
    $scope.sumoMatchApi = sumoMatchApi;
    $scope.robotApi = robotApi;

	$scope.getAll = function() {
		sumoMatchService.getByCompetitionGuid($scope.competition.guid)
		.then(function(result) {
			$scope.sumoMatches = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista meczów sumo nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
