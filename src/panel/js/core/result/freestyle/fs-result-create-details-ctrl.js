app.controller("createFsResultDetailsCtrl",
	["$scope", "$routeParams", "alertService", "fsResultService", "fsResultApi", "$q", "fsCompetitionService",
	function ($scope, $routeParams, alertService, fsResultService, fsResultApi, $q, fsCompetitionService) {

	$scope.places = [{
		name: "Pierwsze",
		value: "FIRST"
	}, {
		name: "Drugie",
		value: "SECOND"
	}, {
		name: "Trzecie",
		value: "THIRD"
	}];

	$scope.freestyleResult = {
		selectedPlace: $scope.places[0]
	};
	$scope.competition = {
		guid: $routeParams.competitionGuid
	};

	$scope.save = function(freestyleResult) {
		var data = angular.copy(freestyleResult);
		data.place = {
			value: data.selectedPlace.value
		};
		data.competitionGuid = data.selectedCompetition.guid;
		data.robotGuid = freestyleResult.selectedRobot.guid;

		delete data.selectedPlace;
		delete data.selectedRobot;
		delete data.selectedCompetition;

		fsResultService.create(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			fsResultApi.goToList($scope.competition.guid);
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
		});
	};

	$q.all([
		fsCompetitionService.getAll()
	]).then(function(result) {
		$scope.competitions = result[0].data;
		if($scope.competitions.length > 0) {
			$scope.freestyleResult.selectedCompetition = $scope.competitions[0];
		}
	});

	$scope.searchRobots = function(search) {
		if($scope.freestyleResult.selectedCompetition) {
			fsCompetitionService.searchRobotInCompetitionMatchingAnyParam($scope.freestyleResult.selectedCompetition.guid, search)
			.then(function(result) {
				if(result !== null) {
					$scope.availableRobots = result.data;
				}
			});
		}
	};
}]);
