app.service("fsResultService", ["$http", "contextService", function ($http, contextService) {

	this.create = function(result) {
		var url = "/result/freestyle";
		return $http.post(contextService.insertTournamentEditionContext(url), result);
	};

	this.update = function(result) {
		var url = "/result/freestyle/guid/" + result.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), result);
	};

	this.delete = function(guid) {
		var url = "/result/freestyle/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/result/freestyle/all"
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/result/freestyle/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByCompetition = function(guid) {
		var url = "/result/freestyle/competition/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByRobot = function(guid) {
		var url = "/result/freestyle/robot/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};
}]);
