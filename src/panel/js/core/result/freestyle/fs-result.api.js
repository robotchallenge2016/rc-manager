app.constant("FS_RESULT_API_CONSTANTS", {
    fsCompetitionList: "/tournament/result/freestyle",
    list: "/tournament/result/freestyle/competition/:competitionGuid",
    create: "/tournament/result/freestyle/competition/:competitionGuid/create",
    update: "/tournament/result/freestyle/competition/:competitionGuid/guid/:resultGuid"
});

app.service("fsResultApi", ["$location", "FS_RESULT_API_CONSTANTS", function($location, FS_RESULT_API_CONSTANTS) {

    this.goToFsCompetitionList = function() {
        $location.path(FS_RESULT_API_CONSTANTS.fsCompetitionList);
    };

    this.goToList = function(competitionGuid) {
        $location.path(FS_RESULT_API_CONSTANTS.list.replace(":competitionGuid", competitionGuid));
    };

    this.goToCreate = function(competitionGuid) {
        $location.path(FS_RESULT_API_CONSTANTS.create.replace(":competitionGuid", competitionGuid));
    };

    this.goToUpdate = function(competitionGuid, resultGuid) {
        $location.path(FS_RESULT_API_CONSTANTS.update.replace(":competitionGuid", competitionGuid)
                                                        .replace(":resultGuid", resultGuid));
    };
}]);
