app.config(["$routeProvider", "FS_RESULT_API_CONSTANTS", function ($routeProvider, FS_RESULT_API_CONSTANTS) {
	$routeProvider

	// Freestyle Result
	.when(FS_RESULT_API_CONSTANTS.fsCompetitionList, {
		templateUrl: "views/core/result/freestyle/fs-competition-result-list.view.html",
		controller: "fsCompetitionListCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(FS_RESULT_API_CONSTANTS.list, {
		templateUrl: "views/core/result/freestyle/fs-result-list.view.html",
		controller: "fsResultListCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(FS_RESULT_API_CONSTANTS.create, {
		templateUrl: "views/core/result/freestyle/fs-result-create-details.view.html",
		controller: "createFsResultDetailsCtrl",
		authz: {
			isAuthorized: "USER"
		}
	})
	.when(FS_RESULT_API_CONSTANTS.update, {
		templateUrl: "views/core/result/freestyle/fs-result-update-details.view.html",
		controller: "updateFsResultDetailsCtrl",
		authz: {
			isAuthorized: "USER"
		}
	});
}]);
