app.controller("fsResultListCtrl",
    ["$scope", "$routeParams", "$uibModal", "fsResultService", "fsResultApi", "alertService",
    function ($scope, $routeParams, $uibModal, fsResultService, fsResultApi, alertService) {

    $scope.competition = {
        guid: $routeParams.competitionGuid
    };
	$scope.freestyleResults = [];
    $scope.fsResultApi = fsResultApi;

	$scope.open = function (freestyleResult) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/result/result-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return freestyleResult;
	      	}
	      }
	    });

		removeModal.result
		.then(function (freestyleResult) {
			$scope.delete(freestyleResult.guid);
	    });
	};

	$scope.delete = function(guid) {
		fsResultService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		});
	};

	$scope.getAll = function() {
		fsResultService.getByCompetition($scope.competition.guid)
		.then(function(result) {
			$scope.freestyleResults = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista wyników nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
