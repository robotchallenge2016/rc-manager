app.controller("updateFsResultDetailsCtrl",
	["$scope", "$routeParams", "$location", "$q", "fsResultService", "robotService", "alertService", "fsCompetitionService",
	function ($scope, $routeParams, $location, $q, fsResultService, robotService, alertService, fsCompetitionService) {

	$scope.places = [{
		name: "Pierwsze",
		value: "FIRST"
	}, {
		name: "Drugie",
		value: "SECOND"
	}, {
		name: "Trzecie",
		value: "THIRD"
	}];

	$scope.freestyleResult = {
		guid: $routeParams.resultGuid
	};
	$scope.memento = {};

	$scope.save = function(freestyleResult) {
		var data = angular.copy(freestyleResult);
		data.place = {
			value: data.selectedPlace.value
		};
		data.competitionGuid = data.selectedCompetition.guid;
		data.robotGuid = freestyleResult.selectedRobot.guid;

		delete data.selectedPlace;
		delete data.selectedRobot;
		delete data.selectedCompetition;

		fsResultService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			$location.path("/tournament/result/freestyle");
		});
	};

	$scope.searchRobots = function(search) {
		if($scope.freestyleResult.selectedCompetition) {
			fsCompetitionService.searchRobotInCompetitionMatchingAnyParam($scope.freestyleResult.selectedCompetition.guid, search)
			.then(function(result) {
				if(result !== null) {
					$scope.availableRobots = result.data;
				}
			});
		}
	};

	$q.all([
		fsCompetitionService.getAll()
	]).then(function(result) {
		$scope.competitions = result[0].data;
		if($scope.competitions.length > 0) {
			$scope.freestyleResult.selectedCompetition = $scope.competitions[0];
		}
	});

	fsResultService.getByGuid($scope.freestyleResult.guid)
	.then(function(result) {
		$scope.freestyleResult = result.data;
		$scope.memento.freestyleResult = angular.copy($scope.freestyleResult);

		$scope.freestyleResult.selectedPlace = $scope.places.filter(function(place) {
    		return place.value == result.data.place.value;
    	})[0];

		if(result.data.competitionGuid) {
			fsCompetitionService.getByGuid(result.data.competitionGuid)
			.then(function(result) {
				$scope.freestyleResult.selectedCompetition = result.data;
			})
			.catch(function(error) {
				alertService.warn("Dane o konkurencji nie zostały pobrane !");
			});
		}

		if(result.data.robotGuid) {
			robotService.getByGuid(result.data.robotGuid)
			.then(function(result) {
				$scope.freestyleResult.selectedRobot = result.data;
			})
			.catch(function(error) {
				alertService.warn("Dane o robocie nie zostały pobrane !");
			});
		}
	})
	.catch(function(error) {
		alertService.warn("Nie udało się pobrać szczegółów wyniku !");
	});
}]);
