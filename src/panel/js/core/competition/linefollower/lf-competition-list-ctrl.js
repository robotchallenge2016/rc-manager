app.controller("lfCompetitionListCtrl",
    ["$scope", "$uibModal", "lfCompetitionService", "lfCompetitionApi", "competitionApi", "lfResultApi", "rankingApi", "alertService",
    function ($scope, $uibModal, competitionService, lfCompetitionApi, competitionApi, lfResultApi, rankingApi, alertService) {

	$scope.competitions = [];
    $scope.rankingApi = rankingApi;
    $scope.competitionApi = competitionApi;
    $scope.lfCompetitionApi = lfCompetitionApi;
    $scope.lfResultApi = lfResultApi;

	$scope.open = function (competition) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/competition/competition-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return competition;
	      	}
	      }
	    });

		removeModal.result
		.then(function (competition) {
			$scope.delete(competition.guid);
	    });
	};

	$scope.delete = function(guid) {
		competitionService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Konkurencja nie została usunieta !");
		});
	};

    $scope.getAllCompetitionSubTypes = function() {

    };

	$scope.getAll = function() {
		competitionService.getAll()
		.then(function(result) {
			$scope.competitions = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista konkurencji nie została pobrana !");
		});
	};

    $scope.startCompetition = function(competition) {
        competitionService.startCompetition(competition)
		.then(function(result) {
			alertService.success("Konkurencja została rozpoczęta !");
            $scope.getAll();
		});
    };

    $scope.stopCompetition = function(competition) {
        competitionService.stopCompetition(competition)
		.then(function(result) {
			alertService.success("Konkurencja została zatrzymana !");
            $scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Nie udało się zatrzymać konkurencji !");
		});
    };

    $scope.getAll();
}]);
