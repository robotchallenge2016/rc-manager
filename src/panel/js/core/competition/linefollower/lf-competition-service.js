app.service("lfCompetitionService", ["$http", "$q", "contextService", function ($http, $q, contextService) {

	this.create = function(competition) {
		var url = "/competition/linefollower";
		return $http.post(contextService.insertTournamentEditionContext(url), competition);
	};

	this.update = function(competition) {
		var url = "/competition/linefollower/guid/" + competition.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), competition);
	};

	this.delete = function(guid) {
		var url = "/competition/linefollower/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/competition/linefollower/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByCompetitorGuid = function(guid) {
		var url = "/competition/linefollower/competitor/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.searchRobotInCompetitionMatchingAnyParam = function(guid, search) {
		if(search.length > 2) {
			var params = {search: search};
			var url = "/competition/linefollower/guid/" + guid + "/robot/match/any";
			return $http.get(contextService.insertTournamentEditionContext(url), {params: params});
		}
		return $q.when(null);
	};

	this.getByGuid = function(guid) {
		var url = "/competition/linefollower/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.nameIsFree = function(competitionName) {
		if(competitionName !== null) {
			var url = "/competition/linefollower/name/" + competitionName + "/free";
			return $http.get(contextService.insertTournamentEditionContext(url));
		}
		return $q.when(true);
	};

	this.startCompetition = function(competition) {
		var url = "/competition/linefollower/start/guid/" + competition.guid;
        return $http.post(contextService.insertTournamentEditionContext(url), competition);
	};

	this.stopCompetition = function(competition) {
		var url = "/competition/linefollower/stop/guid/" + competition.guid;
		return $http.post(contextService.insertTournamentEditionContext(url), competition);
	};
}]);
