app.config(["$routeProvider", "LF_COMPETITION_API_CONSTANTS", function ($routeProvider, LF_COMPETITION_API_CONSTANTS) {
	$routeProvider

    // Linefollower competition page
	.when(LF_COMPETITION_API_CONSTANTS.list, {
		templateUrl: "views/core/competition/linefollower/lf-competition-list.view.html",
		controller: "lfCompetitionListCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(LF_COMPETITION_API_CONSTANTS.create, {
		templateUrl: "views/core/competition/linefollower/lf-competition-create-details.view.html",
		controller: "createLfCompetitionDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(LF_COMPETITION_API_CONSTANTS.update, {
		templateUrl: "views/core/competition/linefollower/lf-competition-update-details.view.html",
		controller: "updateLfCompetitionDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
