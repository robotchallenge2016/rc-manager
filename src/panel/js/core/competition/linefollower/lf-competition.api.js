app.constant("LF_COMPETITION_API_CONSTANTS", {
    list: "/tournament/competition/linefollower",
    create: "/tournament/competition/linefollower/create",
    update: "/tournament/competition/linefollower/details/:competitionGuid"
});

app.service("lfCompetitionApi",
    ["$location", "LF_COMPETITION_API_CONSTANTS",
    function($location, LF_COMPETITION_API_CONSTANTS) {

    this.goToList = function() {
        $location.path(LF_COMPETITION_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(LF_COMPETITION_API_CONSTANTS.create);
    };

    this.goToUpdate = function(competitionGuid) {
        $location.path(LF_COMPETITION_API_CONSTANTS.update.replace(":competitionGuid", competitionGuid));
    };
}]);
