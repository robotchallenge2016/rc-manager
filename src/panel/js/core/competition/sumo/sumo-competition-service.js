app.service("sumoCompetitionService", ["$http", "contextService", function ($http, contextService) {

	this.create = function(competition) {
		var url = "/competition/sumo";
		return $http.post(contextService.insertTournamentEditionContext(url), competition);
	};

	this.update = function(competition) {
		var url = "/competition/sumo/guid/" + competition.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), competition);
	};

	this.delete = function(guid) {
		var url = "/competition/sumo/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/competition/sumo/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByCompetitorGuid = function(guid) {
		var url = "/competition/sumo/competitor/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/competition/sumo/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.nameIsFree = function(competitionName) {
		if(competitionName !== null) {
			var url = "/competition/sumo/name/" + competitionName + "/free";
			return $http.get(contextService.insertTournamentEditionContext(url));
		}
		return $q.when(true);
	};

	this.startCompetition = function(competition) {
		var url = "/competition/sumo/start/guid/" + competition.guid;
        return $http.post(contextService.insertTournamentEditionContext(url), competition);
	};

	this.stopCompetition = function(competition) {
		var url = "/competition/sumo/stop/guid/" + competition.guid;
		return $http.post(contextService.insertTournamentEditionContext(url), competition);
	};

	this.generateOrReplaceSchedule = function(guid) {
		var url = "/competition/sumo/schedule/guid/" + guid;
		return $http.post(contextService.insertTournamentEditionContext(url));
	};
}]);
