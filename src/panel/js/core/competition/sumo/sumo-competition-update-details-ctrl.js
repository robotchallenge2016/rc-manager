app.controller("updateSumoCompetitionDetailsCtrl",
	["$scope", "$routeParams", "$q", "sumoCompetitionService", "sumoCompetitionApi",
		"robotService", "robotApi", "dictionaryService", "alertService", "dictionaryLoadedEventService",
	function ($scope, $routeParams, $q, competitionService, sumoCompetitionApi,
				robotService, robotApi, dictionaryService, alertService, dictionaryLoadedEventService) {

	dictionaryLoadedEventService.listen(initialize);

	$scope.competition = {
		guid: $routeParams.competitionGuid
	};
	$scope.robots = [];
	$scope.memento = {};
	$scope.sumoCompetitionApi = sumoCompetitionApi;
	$scope.robotApi = robotApi;

	$scope.search = {
		selectedRobot: {},
		availableRobots: []
	};

	initialize();

	function initialize() {
		$scope.competitionSubtypes = dictionaryService.competitionSubtypeByCompetitionType("SUMO");
	}

	$scope.$watch("competition.competitionSubtype", function(newValue, oldValue) {
		if(newValue) {
			removeAllRobots();
		}
	});

	$scope.save = function(competition) {
		var data = angular.copy(competition);
		data.competitionSubtypeGuid = competition.competitionSubtype.guid;
		data.robotGuids = [];
		for (var i = 0; i < $scope.robots.length; i++) {
			data.robotGuids[i] = $scope.robots[i].guid;
		}
		delete data.robots;
		delete data.competitionType;

		competitionService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			sumoCompetitionApi.goToList();
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
		});
	};

	$scope.competitionNameIsFree = function(competitionName) {
		if(competitionName != $scope.memento.competition.name) {
			return competitionService.nameIsFree(competitionName);
		}
		return $q.when(true);
	};

	$scope.searchRobots = function(search) {
		if($scope.competition.competitionSubtype) {
			robotService.searchByTypeMatchingAnyParam($scope.competition.competitionSubtype.robotType.guid, search)
			.then(function(result) {
				if(result !== null) {
					var availableRobots = result.data;
					for(var i = 0; i < $scope.robots.length; ++i) {
						var robot = $scope.robots[i];
						availableRobots = availableRobots.filter(function(foundRobot) {
							return robot.guid !== foundRobot.guid;
						});
					}
					$scope.search.availableRobots = availableRobots;
				}
			});
		}
	};

	$scope.addRobot = function(robot) {
		$scope.robots.push(robot);
	};

	$scope.removeRobot = function(robot) {
		$scope.robots = $scope.robots.filter(function (r) {
		  return r.guid !== robot.guid;
		});
		$scope.competitionForm.$setDirty();
	};

	function removeAllRobots() {
		$scope.search.selectedRobot = {};
		$scope.search.availableRobots = [];
		$scope.robots = [];
	}

	competitionService.getByGuid($scope.competition.guid)
	.then(function(result) {
		$scope.competition = result.data;
		$scope.competition.competitionSubtype = dictionaryService.competitionSubtypeByGuid($scope.competition.competitionSubtypeGuid);
		$scope.memento.competition = angular.copy($scope.competition);
		if(result.data.robotGuids) {
			robotService.getByGuids(result.data.robotGuids)
			.then(function(result) {
				$scope.robots = result.data;
			})
			.catch(function(error) {
				alertService.warn("Lista robotów nie została pobrana !");
			});
		}
	})
	.catch(function(error) {
		alertService.warn("Nie udało się pobrać szczegółów konkurencji !");
	});
}]);
