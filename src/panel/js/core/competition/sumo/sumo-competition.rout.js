app.config(["$routeProvider", "SUMO_COMPETITION_API_CONSTANTS", function ($routeProvider, SUMO_COMPETITION_API_CONSTANTS) {
	$routeProvider

    // Sumo competition page
	.when(SUMO_COMPETITION_API_CONSTANTS.list, {
		templateUrl: "views/core/competition/sumo/sumo-competition-list.view.html",
		controller: "sumoCompetitionListCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(SUMO_COMPETITION_API_CONSTANTS.create, {
		templateUrl: "views/core/competition/sumo/sumo-competition-create-details.view.html",
		controller: "createSumoCompetitionDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(SUMO_COMPETITION_API_CONSTANTS.update, {
		templateUrl: "views/core/competition/sumo/sumo-competition-update-details.view.html",
		controller: "updateSumoCompetitionDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
