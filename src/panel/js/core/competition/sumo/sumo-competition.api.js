app.constant("SUMO_COMPETITION_API_CONSTANTS", {
    list: "/tournament/competition/sumo",
    create: "/tournament/competition/sumo/create",
    update: "/tournament/competition/sumo/details/:competitionGuid"
});

app.service("sumoCompetitionApi",
    ["$location", "SUMO_COMPETITION_API_CONSTANTS",
    function($location, SUMO_COMPETITION_API_CONSTANTS) {

    this.goToList = function() {
        $location.path(SUMO_COMPETITION_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(SUMO_COMPETITION_API_CONSTANTS.create);
    };

    this.goToUpdate = function(sumoCompetitionGuid) {
        $location.path(SUMO_COMPETITION_API_CONSTANTS.update.replace(":competitionGuid", sumoCompetitionGuid));
    };
}]);
