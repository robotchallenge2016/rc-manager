app.controller("sumoCompetitionListCtrl",
    ["$scope", "$uibModal", "sumoCompetitionService", "sumoCompetitionApi", "competitionApi", "sumoMatchApi", "rankingApi", "alertService",
    function ($scope, $uibModal, competitionService, sumoCompetitionApi, competitionApi, sumoMatchApi, rankingApi, alertService) {

	$scope.competitions = [];
    $scope.sumoCompetitionApi = sumoCompetitionApi;
    $scope.competitionApi = competitionApi;
    $scope.sumoMatchApi = sumoMatchApi;
    $scope.rankingApi = rankingApi;

	$scope.open = function (competition) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/competition/competition-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return competition;
	      	}
	      }
	    });

		removeModal.result
		.then(function (competition) {
			$scope.delete(competition.guid);
	    });
	};

	$scope.delete = function(guid) {
		competitionService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		});
	};

	$scope.getAll = function() {
		competitionService.getAll()
		.then(function(result) {
			$scope.competitions = result.data;
		});
	};

    $scope.startCompetition = function(competition) {
        competitionService.startCompetition(competition)
		.then(function(result) {
			alertService.success("Konkurencja została rozpoczęta !");
            $scope.getAll();
		});
    };

    $scope.generateOrReplaceSchedule = function(competition) {
        competitionService.generateOrReplaceSchedule(competition.guid)
		.then(function(result) {
			alertService.success("Terminarz został utworzony !");
            $scope.getAll();
		});
    };

    $scope.stopCompetition = function(competition) {
        competitionService.stopCompetition(competition)
		.then(function(result) {
			alertService.success("Konkurencja została zatrzymana !");
            $scope.getAll();
		});
    };

    $scope.getAll();
}]);
