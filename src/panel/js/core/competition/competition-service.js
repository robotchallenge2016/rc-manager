app.service("competitionService", ["$http", "contextService", function ($http, contextService) {

	this.create = function(competition) {
		var url = "/competition";
		return $http.post(contextService.insertTournamentEditionContext(url), competition);
	};

	this.update = function(competition) {
		var url = "/competition/guid/" + competition.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), competition);
	};

	this.delete = function(guid) {
		var url = "/competition/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/competition/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByCompetitorGuid = function(guid) {
		var url = "/competition/competitor/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/competition/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.nameIsFree = function(competitionName) {
		if(competitionName !== null) {
			var url = "/competition/name/" + competitionName + "/free";
			return $http.get(contextService.insertTournamentEditionContext(url));
		}
		return $q.when(true);
	};
}]);
