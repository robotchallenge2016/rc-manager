app.controller("competitionListCtrl", ["$scope", "dictionaryService", "competitionApi",
    function ($scope, dictionaryService, competitionApi) {

    $scope.competitionApi = competitionApi;
    $scope.competitionTypes = dictionaryService.competitionTypes;
}]);
