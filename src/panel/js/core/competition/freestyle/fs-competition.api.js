app.constant("FS_COMPETITION_API_CONSTANTS", {
    list: "/tournament/competition/freestyle",
    create: "/tournament/competition/freestyle/create",
    update: "/tournament/competition/freestyle/details/:competitionGuid"
});

app.service("fsCompetitionApi",
    ["$location", "FS_COMPETITION_API_CONSTANTS",
    function($location, FS_COMPETITION_API_CONSTANTS) {

    this.goToList = function() {
        $location.path(FS_COMPETITION_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(FS_COMPETITION_API_CONSTANTS.create);
    };

    this.goToUpdate = function(competitionGuid) {
        $location.path(FS_COMPETITION_API_CONSTANTS.update.replace(":competitionGuid", competitionGuid));
    };
}]);
