app.config(["$routeProvider", function ($routeProvider) {
	$routeProvider

    // Freestyle competition page
    .when("/tournament/competition/freestyle", {
        templateUrl: "views/core/competition/freestyle/fs-competition-list.view.html",
        controller: "fsCompetitionListCtrl",
        authz: {
            isAuthorized: "ADMIN"
        }
    })
    .when("/tournament/competition/freestyle/create", {
        templateUrl: "views/core/competition/freestyle/fs-competition-create-details.view.html",
        controller: "createFsCompetitionDetailsCtrl",
        authz: {
            isAuthorized: "ADMIN"
        }
    })
    .when("/tournament/competition/freestyle/details/:guid", {
        templateUrl: "views/core/competition/freestyle/fs-competition-update-details.view.html",
        controller: "updateFsCompetitionDetailsCtrl",
        authz: {
            isAuthorized: "ADMIN"
        }
    });
}]);
