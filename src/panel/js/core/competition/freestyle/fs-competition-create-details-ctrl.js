app.controller("createFsCompetitionDetailsCtrl",
	["$scope", "$location", "alertService", "fsCompetitionService", "fsCompetitionApi", "robotService", "robotApi",
		"dictionaryService", "dictionaryLoadedEventService",
	function ($scope, $location, alertService, competitionService, fsCompetitionApi, robotService, robotApi,
				dictionaryService, dictionaryLoadedEventService) {

	dictionaryLoadedEventService.listen(initialize);

	var competition = {};
	$scope.competition = competition;
	$scope.robots = [];
	$scope.fsCompetitionApi = fsCompetitionApi;
	$scope.robotApi = robotApi;

	$scope.search = {
		selectedRobot: {},
		availableRobots: []
	};

	initialize();

	function initialize() {
		$scope.competitionSubtypes = dictionaryService.competitionSubtypeByCompetitionType("FREESTYLE");
		if($scope.competitionSubtypes.length > 0) {
			competition.competitionSubtype = $scope.competitionSubtypes[0];
		}
	}

	$scope.$watch("competition.competitionSubtype", function(newValue, oldValue) {
		if(newValue) {
			removeAllRobots();
		}
	});

	$scope.save = function(competition) {
		var data = angular.copy(competition);
		data.competitionSubtypeGuid = competition.competitionSubtype.guid;
		data.robotGuids = [];
		for (var i = 0; i < $scope.robots.length; i++) {
			data.robotGuids[i] = $scope.robots[i].guid;
		}
		delete data.robots;
		delete data.competitionSubtype;

		competitionService.create(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			$location.path("/tournament/competition/freestyle");
		});
	};

	$scope.competitionNameIsFree = function(competitionName) {
		return competitionService.nameIsFree(competitionName);
	};

	$scope.searchRobots = function(search) {
		if($scope.competition.competitionSubtype) {
			robotService.searchByTypeMatchingAnyParam($scope.competition.competitionSubtype.robotType.guid, search)
			.then(function(result) {
				if(result !== null) {
					var availableRobots = result.data;
					for(var i = 0; i < $scope.robots.length; ++i) {
						var robot = $scope.robots[i];
						availableRobots = availableRobots.filter(function(foundRobot) {
							return robot.guid !== foundRobot.guid;
						});
					}
					$scope.search.availableRobots = availableRobots;
				}
			});
		}
	};

	$scope.addRobot = function(robot) {
		$scope.robots.push(robot);
	};

	$scope.removeRobot = function(robot) {
		$scope.robots = $scope.robots.filter(function (r) {
		  return r.guid !== robot.guid;
		});
		$scope.competitionForm.$setDirty();
	};

	function removeAllRobots() {
		$scope.search.selectedRobot = {};
		$scope.search.availableRobots = [];
		$scope.robots = [];
	}
}]);
