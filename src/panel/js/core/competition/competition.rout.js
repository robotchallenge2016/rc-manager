app.config(["$routeProvider", function ($routeProvider) {
	$routeProvider

	// Competition page
	.when("/tournament/competition", {
		templateUrl: "views/core/competition/competition-list.view.html",
		controller: "competitionListCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
