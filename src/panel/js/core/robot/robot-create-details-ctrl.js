app.controller("createRobotDetailsCtrl",
	["$scope", "alertService", "robotService", "robotApi", "competitorService", "systemFileService", "dictionaryService", "dictionaryLoadedEventService",
	function ($scope, alertService, robotService, robotApi, competitorService, systemFileService, dictionaryService, dictionaryLoadedEventService) {

	dictionaryLoadedEventService.listen(initialize);

	var robot = {};

	$scope.robotApi = robotApi;
	$scope.robot = robot;
	$scope.availableCompetitors = [];

	// loader icons
	$scope.saveInProgress = false;

	initialize();

	function initialize() {
		$scope.robotTypes = dictionaryService.robotTypes;
		if($scope.robotTypes.length > 0) {
			robot.robotType = $scope.robotTypes[0];
		}
	}

	$scope.save = function(robot) {
		saveInProgress(true);

		var data = angular.copy(robot);
		data.robotTypeGuid = robot.robotType.guid;
		data.ownerGuid = data.owner.guid;
		delete data.owner;
		delete data.image;

		robotService.create(data)
		.then(function(result) {
			if(imageWasAdded()) {
				systemFileService.upload(result.data.guid, robot.image.file)
				.then(function() {
					alertService.success("Zapisano !");
					robotApi.goToList();
					saveInProgress(false);
				})
				.catch(function() {
					alertService.warn("Zdjęcie robota nie zostało zapisane !");
					saveInProgress(false);
				});
			} else {
				alertService.success("Zapisano !");
				robotApi.goToList();
				saveInProgress(false);
			}
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	$scope.robotNameIsFree = function(robotName) {
		return robotService.nameIsFree(robotName);
	};

	$scope.refreshCompetitors = function (search) {
	    return competitorService.searchMatchingAnyParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableCompetitors = response.data;
			  }
	      });
	};

	function imageWasAdded() {
		if(typeof $scope.robot.image !== "undefined" && $scope.robot.image !== null) {
			if(typeof $scope.robot.image.file !== "undefined" && $scope.robot.image.file !== null) {
				return true;
			}
		}
		return false;
	}

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
