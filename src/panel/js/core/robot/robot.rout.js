app.config(["$routeProvider", "ROBOT_API_CONSTANTS", function ($routeProvider, ROBOT_API_CONSTANTS) {
	$routeProvider
    .when(ROBOT_API_CONSTANTS.list, {
		templateUrl: "views/core/robot/robot-list.view.html",
		controller: "robotListCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(ROBOT_API_CONSTANTS.create, {
		templateUrl: "views/core/robot/robot-create-details.view.html",
		controller: "createRobotDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(ROBOT_API_CONSTANTS.update, {
		templateUrl: "views/core/robot/robot-update-details.view.html",
		controller: "updateRobotDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
