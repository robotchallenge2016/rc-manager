app.constant("ROBOT_API_CONSTANTS", {
    list: "/tournament/robot",
    create: "/tournament/robot/create",
    update: "/tournament/robot/details/:robotGuid"
});

app.service("robotApi", ["$location", "ROBOT_API_CONSTANTS", function($location, ROBOT_API_CONSTANTS) {
    this.goToList = function() {
        $location.path(ROBOT_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(ROBOT_API_CONSTANTS.create);
    };

    this.goToUpdate = function(robotGuid) {
        $location.path(ROBOT_API_CONSTANTS.update.replace(":robotGuid", robotGuid));
    };
}]);
