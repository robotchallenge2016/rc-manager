app.controller("updateRobotDetailsCtrl",
	["$scope", "$routeParams", "$location", "$q", "robotService", "robotApi", "competitorService", "systemFileService", "dictionaryService", "alertService",
	function ($scope, $routeParams, $location, $q, robotService, robotApi, competitorService, systemFileService, dictionaryService, alertService) {

	var robot = {
		guid: $routeParams.robotGuid
	};

	$scope.robotApi = robotApi;
	$scope.robot = robot;
	$scope.memento = {};

	// loader icons
	$scope.saveInProgress = false;
	$scope.robotTypes = dictionaryService.robotTypes;

	$q.all([
		robotService.getByGuid(robot.guid),
		competitorService.searchRobotOwner(robot.guid),
	]).then(function(result) {
		var robot = result[0].data;
		robot.owner = result[1].data;
		robot.image = $scope.robot.image;
		robot.robotType = dictionaryService.robotTypeByGuid(robot.robotTypeGuid);
		$scope.robot = robot;

		$scope.memento.robot = angular.copy($scope.robot);
	});

	$scope.$watch("robot.image.file", function(newValue, oldValue) {
		if(typeof newValue !== "undefined" && newValue !== null) {
			$scope.robotForm.robotImage.$setDirty();
		}
	});

	$scope.save = function(robot) {
		saveInProgress(true);

		var data = angular.copy(robot);
		data.ownerGuid = data.owner.guid;
		data.robotTypeGuid = data.robotType.guid;
		delete data.owner;
		delete data.robotType;
		delete data.image;

		robotService.update(data)
		.then(function() {
			if(imageWasAdded()) {
                updateImageOfRobot(robot);
			} else {
				alertService.success("Zapisano !");
				robotApi.goToList();
				saveInProgress(false);
			}
		})
		.catch(function() {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	function updateImageOfRobot(robot) {
        systemFileService.upload(robot.guid, robot.image.file)
            .then(function() {
                alertService.success("Zapisano !");
                robotApi.goToList();
                saveInProgress(false);
            })
            .catch(function() {
                alertService.warn("Zdjęcie robota nie zostało zapisane !");
                saveInProgress(false);
            });
	}

	$scope.robotNameIsFree = function(robotName) {
		if(robotName != $scope.memento.robot.name) {
			return robotService.nameIsFree(robotName);
		}
		return $q.when(true);
	};

	$scope.refreshCompetitors = function (search) {
	    return competitorService.searchMatchingAnyParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableCompetitors = response.data;
			  }
	      });
	};

	function imageWasAdded() {
		if(typeof $scope.robot.image !== "undefined" && $scope.robot.image !== null) {
			if(typeof $scope.robot.image.file !== "undefined" && $scope.robot.image.file !== null) {
				return true;
			}
		}
		return false;
	}

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
