app.service("robotService", ["$http", "$q", "contextService", function ($http, $q, contextService) {

	this.create = function(robot) {
		var url = "/robot";
		return $http.post(contextService.insertTournamentEditionContext(url), robot);
	};

	this.update = function(robot) {
		var url = "/robot/guid/" + robot.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), robot);
	};

	this.delete = function(guid) {
		var url = "/robot/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAllBriefed = function() {
		var url = "/robot/all/brief";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getAllFull = function() {
		var url = "/robot/all/full";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/robot/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByType = function(robotTypeGuid) {
		var url = "/robot/type/guid/" + robotTypeGuid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuids = function(guids) {
		var url = "/robot/guids";
		return $http.post(contextService.insertTournamentEditionContext(url), {guids: guids});
	};

	this.getByCompetitorGuid = function(competitorGuid) {
		var url = "/robot/competitor/guid/" + competitorGuid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByTeamGuid = function(teamGuid) {
		var url = "/robot/team/guid/" + teamGuid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.nameIsFree = function(robotName) {
		if(robotName !== null) {
			var url = "/robot/name/" + robotName + "/free";
			return $http.get(contextService.insertTournamentEditionContext(url));
		}
		return $q.when(true);
	};

	this.searchMatchingAnyParam = function(search) {
		if(search.length > 2) {
			var params = {search: search};
			var url = "/robot/match/any";
			return $http.get(contextService.insertTournamentEditionContext(url), {params: params});
		}
		return $q.when(null);
	};

	this.searchByTypeMatchingAnyParam = function(robotTypeGuid, search) {
		if(search.length > 2) {
			var params = {search: search};
			var url = "/robot/type/guid/" + robotTypeGuid + "/match/any";
			return $http.get(contextService.insertTournamentEditionContext(url), {params: params});
		}
		return $q.when(null);
	};
}]);
