app.controller("robotListCtrl",
    ["$scope", "$uibModal", "robotService", "robotApi", "alertService", "uiGridConstants",
    function ($scope, $uibModal, robotService, robotApi, alertService, uiGridConstants) {

    $scope.robotApi = robotApi;
	$scope.robots = [];

    $scope.gridOptions = {
        enableFiltering: true,
        minRowsToShow: 15,
        showGridFooter: true,
        onRegisterApi: function(gridApi){
          $scope.gridApi = gridApi;
        },
        columnDefs: [
          { field: 'name', name: 'Nazwa' },
          { field: 'robotType.name', name: 'Typ' },
          { field: 'owner.login', name: 'Uczestnik' },
          { field: 'owner.teamName', name: 'Zespół' },
          { name: 'Menu', enableFiltering: false, enableSorting: false, enableHiding: false, enableColumnMenu: false,
            cellTemplate:
          '<div style="display: inline-block;">' +
  			'<a class="btn btn-xs btn-info btn-menu" href="javascript:;" ng-click="grid.appScope.robotApi.goToUpdate(row.entity.guid)" uib-tooltip="Edytuj">' +
  	           '<span class="glyphicon glyphicon-cog"></span> ' +
               '<strong>Edytuj</strong>' +
            '</a>' +
            '<a class="btn btn-xs btn-danger btn-menu" ng-click="grid.appScope.open(row.entity)" uib-tooltip="Usuń">' +
                '<span class="glyphicon glyphicon-trash"></span> ' +
                '<strong>Usuń</strong>' +
            '</a>' +
          '</div>'
            }
        ]
    };

	$scope.open = function (robot) {
		var removeModal = $uibModal.open({
	      templateUrl: "views/core/robot/robot-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return robot;
	      	}
	      }
	    });

		removeModal.result
		.then(function (robot) {
			$scope.delete(robot.guid);
	    });
	};

	$scope.delete = function(guid) {
		robotService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		});
	};

	$scope.getAll = function() {
		robotService.getAllBriefed()
		.then(function(result) {
			$scope.robots = result.data;
            $scope.gridOptions.data = result.data;
		});
	};

	$scope.getAll();
}]);
