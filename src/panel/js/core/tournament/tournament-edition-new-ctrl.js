app.controller("tournamentEditionNewCtrl", [
    "$scope", "$location", "$timeout", "$window", "alertService", "homeRobotApi", "tournamentEditionService",
    function($scope, $location, $timeout, $window, alertService, homeRobotApi, tournamentEditionService) {

        $scope.settings = {
            registrationOpened: false,
            tournamentStarted: false,
            maxLineFollowerPerCompetitor: 0,
            maxSumoPerCompetitor: 0,
            maxFreestylePerCompetitor: 0
        };
        $scope.suffixIsFree = tournamentEditionService.suffixIsFree;

        $scope.save = function(settings) {
            tournamentEditionService.create(settings)
            .then(function() {
                alertService.success("Zapisano !");
                $timeout(function() {
                    $window.location.reload();
                    homeRobotApi.goToList();
                }, 500);
            });
        };
}]);
