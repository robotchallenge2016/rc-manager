app.service("tournamentService", ["$http", "$q", "contextService", function ($http, $q, contextService) {

    this.create = function(tournament) {
        var url = "/";
        return $http.post(contextService.insertTournamentContext(url), tournament);
    };

    this.update = function(tournament) {
        var url = "/guid/" + tournament.guid;
        return $http.put(contextService.insertTournamentContext(url), tournament);
    };

    this.retrieve = function(guid) {
        var url = "/guid/" + guid;
        return $http.get(contextService.insertTournamentContext(url));
    };

    this.nameIsFree = function(name) {
        if(name !== null) {
            var url = "/name/" + name + "/free";
            return $http.get(contextService.insertTournamentContext(url));
        }
        return $q.when(true);
    };

    this.domainIsFree = function(domain) {
        if(name !== null) {
            var url = "/domain/" + domain + "/free";
            return $http.get(contextService.insertTournamentContext(url));
        }
        return $q.when(true);
    };
}]);
