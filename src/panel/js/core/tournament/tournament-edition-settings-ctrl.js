app.controller("tournamentEditionSettingsCtrl", [
    "$scope", "$location", "$q", "$timeout", "$uibModal", "$window", "alertService", "homeRobotApi", "tournamentEditionService",
    function($scope, $location, $q, $timeout, $uibModal, $window, alertService, homeRobotApi, tournamentEditionService) {

        $scope.memento = {};

        tournamentEditionService.retrieve()
        .then(function(result) {
            $scope.settings = result.data;
            $scope.memento.settings = angular.copy($scope.settings);
        });

        $scope.save = function(settings) {
            tournamentEditionService.update(settings)
            .then(function() {
                alertService.success("Zapisano !");
                $timeout(function() {
                    $window.location.reload();
                    homeRobotApi.goToList();
                }, 500);
            });
        };

        $scope.suffixIsFree = function(suffix) {
            if($scope.memento.settings.nameSuffix !== suffix) {
                return tournamentEditionService.suffixIsFree(suffix);
            }
            return $q.when(true);
        };

        $scope.open = function () {

            var removeModal = $uibModal.open({
                templateUrl: "views/core/tournament/tournament-edition-remove.view.html",
                controller: "entityRemoveCtrl",
                resolve: {
                    entity: function () {
                        return $scope.context;
                    }
                }
            });

            removeModal.result
                .then(function () {
                    tournamentEditionService.delete()
                        .then(function() {
                            alertService.success("Usunięto !");
                            $timeout(function() {
                                $window.location.reload();
                                homeRobotApi.goToList();
                            }, 500);
                        });
                });
        };
}]);
