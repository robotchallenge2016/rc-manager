app.controller("tournamentSettingsCtrl", [
    "$scope", "$location", "$q", "$timeout", "$window", "alertService", "contextService", "homeRobotApi", "tournamentService",
    function($scope, $location, $q, $timeout, $window, alertService, contextService, homeRobotApi, tournamentService) {

        $scope.memento = {};

        tournamentService.retrieve(contextService.context.tournament.guid)
        .then(function(result) {
            $scope.tournament = result.data;
            $scope.memento.tournament = angular.copy($scope.tournament);
        });

        $scope.save = function(tournament) {
            tournamentService.update(tournament)
            .then(function() {
                alertService.success("Zapisano !");
                $timeout(function() {
                    $window.location.reload();
                    homeRobotApi.goToList();
                }, 500);
            });
        };

        $scope.nameIsFree = function(name) {
            if($scope.memento.tournament.name !== name) {
                return tournamentService.nameIsFree(name);
            }
            return $q.when(true);
        };

        $scope.domainIsFree = function(domain) {
            if($scope.memento.tournament.domain !== domain) {
                return tournamentService.domainIsFree(domain);
            }
            return $q.when(true);
        };
}]);
