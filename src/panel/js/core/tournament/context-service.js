app.service("contextService", ["$http", function ($http) {

    this.backendContext = "/rc-webapp/v1";
    this.tournamentContext = this.backendContext + "/tournament";
    this.tournamentAndEditionContext = this.tournamentContext + "/edition/guid/{editionGuid}";
    
    this.context = {

    };

    this.insertContext = function(urlTemplate) {
        return (this.backendContext + urlTemplate);
    };

    this.insertTournamentContext = function(urlTemplate) {
        return (this.tournamentContext + urlTemplate);
    };

    this.insertTournamentEditionContext = function(urlTemplate) {
        return (this.tournamentAndEditionContext + urlTemplate)
            .replace("{editionGuid}", this.context.selectedEdition.guid);
    };

    this.retrieveContext = function() {
        return $http.get("/rc-webapp/v1/context/tournament");
    };

    this.retrieveEditionsContext = function() {
        return $http.get("/rc-webapp/v1/context/tournament/edition");
    };
}]);
