app.service("tournamentEditionService", ["$http", "$q", "contextService", function ($http, $q, contextService) {

    this.create = function(edition) {
        var url = "/edition";
        return $http.post(contextService.insertTournamentContext(url), edition);
    };

	this.update = function(edition) {
		var url = "/";
		return $http.put(contextService.insertTournamentEditionContext(url), edition);
	};

    this.delete = function() {
        var url = "/";
        return $http.delete(contextService.insertTournamentEditionContext(url));
    };

	this.retrieve = function() {
		var url = "/";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.suffixIsFree = function(suffix) {
	    if(suffix !== null) {
            var url = "/edition/suffix/" + suffix + "/free";
            return $http.get(contextService.insertTournamentContext(url));
        }
        return $q.when(true);
    }
}]);
