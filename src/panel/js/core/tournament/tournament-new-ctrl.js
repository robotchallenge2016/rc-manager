app.controller("tournamentNewCtrl", [
    "$scope", "$location", "$timeout", "$window", "alertService", "homeRobotApi", "tournamentService",
    function($scope, $location, $timeout, $window, alertService, homeRobotApi, tournamentService) {

        $scope.nameIsFree = tournamentService.nameIsFree;
        $scope.domainIsFree = tournamentService.domainIsFree;

        $scope.save = function(tournament) {
            tournamentService.create(tournament)
            .then(function() {
                alertService.success("Zapisano !");
                $timeout(function() {
                    $window.location.reload();
                    homeRobotApi.goToList();
                }, 500);
            });
        };
}]);
