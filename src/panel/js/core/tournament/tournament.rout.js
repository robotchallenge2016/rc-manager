app.config(["$routeProvider", "TOURNAMENT_API_CONSTANTS", function ($routeProvider, TOURNAMENT_API_CONSTANTS) {
	$routeProvider

	.when(TOURNAMENT_API_CONSTANTS.create, {
		templateUrl: "views/core/tournament/tournament-new.view.html",
		controller: "tournamentNewCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(TOURNAMENT_API_CONSTANTS.update, {
		templateUrl: "views/core/tournament/tournament-settings.view.html",
		controller: "tournamentSettingsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(TOURNAMENT_API_CONSTANTS.createEdition, {
		templateUrl: "views/core/tournament/tournament-edition-new.view.html",
		controller: "tournamentEditionNewCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(TOURNAMENT_API_CONSTANTS.updateEdition, {
		templateUrl: "views/core/tournament/tournament-edition-settings.view.html",
		controller: "tournamentEditionSettingsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
