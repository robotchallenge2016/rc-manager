app.constant("TOURNAMENT_API_CONSTANTS", {
    create: "/tournament/new",
    update: "/tournament/settings",
    createEdition: "/tournament/edition/new",
    updateEdition: "/tournament/edition/settings"
});

app.service("tournamentApi", ["$location", "TOURNAMENT_API_CONSTANTS", function($location, TOURNAMENT_API_CONSTANTS) {

    this.goToCreate = function() {
        $location.path(TOURNAMENT_API_CONSTANTS.create);
    };

    this.goToUpdate = function() {
        $location.path(TOURNAMENT_API_CONSTANTS.update);
    };

    this.goToCreateEdition = function() {
        $location.path(TOURNAMENT_API_CONSTANTS.createEdition);
    };

    this.goToUpdateEdition = function() {
        $location.path(TOURNAMENT_API_CONSTANTS.updateEdition);
    };
}]);
