app.directive("focusable", ["$timeout", function ($timeout) {
    return {
        restrict: 'A',
        link: linkFn
    };

    function linkFn(scope, element) {
        $timeout(function() {
            element[0].focus();
        });
    }
}]);
