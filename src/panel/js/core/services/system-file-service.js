app.service("systemFileService", ["$http", "contextService", function($http, contextService) {

    this.upload = function(guid, file) {
        var url = contextService.insertTournamentEditionContext('/robot/' + guid + '/file/upload');
        return $http({
                        method: 'POST',
                        url: url,
                        headers: {
                            'Content-Type': undefined
                        },
                        data: {
                            uploadedFile: file
                        },
                        transformRequest: function (data) {
                            var formData = new FormData();
                            angular.forEach(data, function (value, key) {
                                formData.append(key, value);
                            });

                            return formData;
                        }
                    });
    };
}]);
