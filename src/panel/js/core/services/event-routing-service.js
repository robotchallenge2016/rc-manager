app.service("eventRoutingService",
	["$location", "$location", "$window", "notAuthenticatedEventService", "notAuthorizedEventService",
	function ($location, $location, $window, notAuthenticatedEventService, notAuthorizedEventService) {

	notAuthenticatedEventService.listen(notAuthenticatedCallback);
	notAuthorizedEventService.listen(notAuthorizedCallback);

	function notAuthenticatedCallback() {
		var unencodedTargetUrl = $location.absUrl();
		var encodedTargetUrl;
		if(unencodedTargetUrl) {
			encodedTargetUrl = btoa(unencodedTargetUrl);
		}

		var finalUrl = '/panel/login';
		if(encodedTargetUrl) {
			finalUrl = finalUrl + "/#/?TARGET_URL=" + encodedTargetUrl;
		}
		$window.location.href = finalUrl;
	}

	function notAuthorizedCallback() {
		$location.path("/forbidden");
	}
}]);
