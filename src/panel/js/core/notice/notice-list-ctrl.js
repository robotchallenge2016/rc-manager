app.controller("noticeListCtrl",
    ["$scope", "$location", "$uibModal", "noticeService", "noticeApi", "alertService",
    function ($scope, $location, $uibModal, noticeService, noticeApi, alertService) {

	$scope.notices = [];
    $scope.noticeApi = noticeApi;

	$scope.open = function (notice) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/notice/notice-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return notice;
	      	}
	      }
	    });

		removeModal.result
		.then(function (notice) {
			$scope.delete(notice.guid);
	    });
	};

	$scope.delete = function(guid) {
		noticeService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Ogłoszenie nie zostało usunięte !");
		});
	};

	$scope.getAll = function() {
		noticeService.getAll()
		.then(function(result) {
			$scope.notices = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista ogłoszeń nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
