app.controller("createNoticeDetailsCtrl",
	["$scope", "alertService", "noticeService", "noticeApi",
	function ($scope, alertService, noticeService, noticeApi) {

	var notice = {};
	$scope.notice = notice;
	$scope.noticeApi = noticeApi;

	// loader icons
	$scope.saveInProgress = false;

	$scope.tinymceOptions = {
      onChange: function(e) {
        // put logic here for keypress and cut/paste changes
      },
      inline: false,
      skin: 'lightgray',
      theme : 'modern'
    };

	$scope.save = function(notice) {
		saveInProgress(true);

		noticeService.create(notice)
		.then(function(result) {
			alertService.success("Zapisano !");
			noticeApi.goToList();
			saveInProgress(false);
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
