app.controller("updateNoticeDetailsCtrl",
	["$scope", "$routeParams", "alertService", "noticeService", "noticeApi",
	function ($scope, $routeParams, alertService, noticeService, noticeApi) {

	var notice = {
		guid: $routeParams.noticeGuid
	};
	$scope.notice = notice;
	$scope.noticeApi = noticeApi;

	// loader icons
	$scope.saveInProgress = false;

	$scope.tinymceOptions = {
      onChange: function(e) {
        // put logic here for keypress and cut/paste changes
      },
      inline: false,
    //   plugins : 'advlist autolink link image lists charmap print preview',
      skin: 'lightgray',
      theme : 'modern'
    };

    noticeService.getByGuid(notice.guid)
        .then(function(result) {
            $scope.notice = result.data;
        });

	$scope.save = function(notice) {
		saveInProgress(true);

		noticeService.update(notice)
		.then(function(result) {
			alertService.success("Zapisano !");
			noticeApi.goToList();
			saveInProgress(false);
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
