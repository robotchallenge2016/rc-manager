app.config(["$routeProvider", "NOTICE_API_CONSTANTS", function ($routeProvider, NOTICE_API_CONSTANTS) {
	$routeProvider

    .when(NOTICE_API_CONSTANTS.list, {
        templateUrl: "views/core/notice/notice-list.view.html",
        controller: "noticeListCtrl",
        authz: {
            isAuthorized: "ADMIN"
        }
    })
    .when(NOTICE_API_CONSTANTS.create, {
        templateUrl: "views/core/notice/notice-create-details.view.html",
        controller: "createNoticeDetailsCtrl",
        authz: {
            isAuthorized: "ADMIN"
        }
    })
    .when(NOTICE_API_CONSTANTS.update, {
        templateUrl: "views/core/notice/notice-update-details.view.html",
        controller: "updateNoticeDetailsCtrl",
        authz: {
            isAuthorized: "ADMIN"
        }
    });
}]);
