app.service("noticeService", ["$http", "contextService", function ($http, contextService) {

	this.create = function(notice) {
		var url = "/website/notice";
		return $http.post(contextService.insertTournamentEditionContext(url), notice);
	};

	this.update = function(notice) {
		var url = "/website/notice/guid/" + notice.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), notice);
	};

	this.delete = function(guid) {
		var url = "/website/notice/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/website/notice/all/brief";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/website/notice/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};
}]);
