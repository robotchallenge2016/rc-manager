app.constant("NOTICE_API_CONSTANTS", {
    list: "/website/notice",
    create: "/website/notice/create",
    update: "/website/notice/details/:noticeGuid"
});

app.service("noticeApi", ["$location", "NOTICE_API_CONSTANTS", function($location, NOTICE_API_CONSTANTS) {
    this.goToList = function() {
        $location.path(NOTICE_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(NOTICE_API_CONSTANTS.create);
    };

    this.goToUpdate = function(noticeGuid) {
        $location.path(NOTICE_API_CONSTANTS.update.replace(":noticeGuid", noticeGuid));
    };
}]);
