app.constant("RANKING_API_CONSTANTS", {
    linefollowerRankingList: "/tournament/ranking/linefollower",
    linefollowerRankingDetails: "/tournament/ranking/linefollower/details/:competitionGuid",

    sumoRankingList: "/tournament/ranking/sumo",
    sumoRankingDetails: "/tournament/ranking/sumo/details/:competitionGuid",

    freestyleRankingList: "/tournament/ranking/freestyle",
    freestyleRankingDetails: "/tournament/ranking/freestyle/details/:competitionGuid"
});

app.service("rankingApi", ["$location", "RANKING_API_CONSTANTS", function($location, RANKING_API_CONSTANTS) {
    this.goToLinefollowerRankingList = function() {
        $location.path(RANKING_API_CONSTANTS.linefollowerRankingList);
    };

    this.goToLinefollowerRankingDetails = function(competitionGuid) {
        $location.path(RANKING_API_CONSTANTS.linefollowerRankingDetails.replace(":competitionGuid", competitionGuid));
    };

    this.goToSumoRankingList = function() {
        $location.path(RANKING_API_CONSTANTS.sumoRankingList);
    };

    this.goToSumoRankingDetails = function(competitionGuid) {
        $location.path(RANKING_API_CONSTANTS.sumoRankingDetails.replace(":competitionGuid", competitionGuid));
    };

    this.goToFreestyleRankingList = function() {
        $location.path(RANKING_API_CONSTANTS.freestyleRankingList);
    };

    this.goToFreestyleRankingDetails = function(competitionGuid) {
        $location.path(RANKING_API_CONSTANTS.freestyleRankingDetails.replace(":competitionGuid", competitionGuid));
    };
}]);
