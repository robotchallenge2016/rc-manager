app.service("rankingService", ["$http", "contextService", function ($http, contextService) {

	this.linefollowerRanking = function(guid) {
		var url = "/ranking/linefollower/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

    this.sumoRanking = function(guid) {
    	var url = "/ranking/sumo/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

    this.freestyleRanking = function(guid) {
    	var url = "/ranking/freestyle/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};
}]);
