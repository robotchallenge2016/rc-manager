app.controller("sumoRankingCtrl",
    ["$scope", "$routeParams", "rankingService", "rankingApi", "alertService",
    function ($scope, $routeParams, rankingService, rankingApi, alertService) {

	$scope.rankings = [];
    $scope.rankingApi = rankingApi;

	$scope.getAll = function() {
		rankingService.sumoRanking($routeParams.competitionGuid)
		.then(function(result) {
			$scope.rankings = result.data;
		})
		.catch(function(error) {
			alertService.warn("Ranking nie został pobrany !");
		});
	};

	$scope.getAll();
}]);
