app.config(["$routeProvider", "RANKING_API_CONSTANTS", function ($routeProvider, RANKING_API_CONSTANTS) {
	$routeProvider

	.when(RANKING_API_CONSTANTS.linefollowerRankingList, {
		templateUrl: "views/core/ranking/lf-ranking-list.view.html",
		controller: "lfCompetitionListCtrl"
	})

	.when(RANKING_API_CONSTANTS.linefollowerRankingDetails, {
		templateUrl: "views/core/ranking/lf-ranking.view.html",
		controller: "lfRankingCtrl"
	})

	// Sumo competition ranking page
	.when(RANKING_API_CONSTANTS.sumoRankingList, {
		templateUrl: "views/core/ranking/sumo-ranking-list.view.html",
		controller: "sumoCompetitionListCtrl"
	})
	.when(RANKING_API_CONSTANTS.sumoRankingDetails, {
		templateUrl: "views/core/ranking/sumo-ranking.view.html",
		controller: "sumoRankingCtrl"
	})

	// Freestyle competition ranking page
	.when(RANKING_API_CONSTANTS.freestyleRankingList, {
		templateUrl: "views/core/ranking/fs-ranking-list.view.html",
		controller: "fsCompetitionListCtrl"
	})

	.when(RANKING_API_CONSTANTS.freestyleRankingDetails, {
		templateUrl: "views/core/ranking/fs-ranking.view.html",
		controller: "fsRankingCtrl"
	});
}]);
