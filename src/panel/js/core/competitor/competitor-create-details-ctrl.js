app.controller("createCompetitorDetailsCtrl",
	["$scope", "alertService", "competitorService", "competitorApi", "teamService",
	function ($scope, alertService, competitorService, competitorApi, teamService) {

	var competitor = {};
	$scope.competitor = competitor;
	$scope.competitorApi = competitorApi;

	var noTeam = { name: "Brak", guid: null };
	$scope.noTeam = noTeam;
	$scope.availableTeams = [ noTeam ];

	// loader icons
	$scope.saveInProgress = false;

	$scope.removeTeam = function() {
		$scope.competitor.team = null;
		$scope.competitorForm.selectedTeam.$setDirty();
	};

	$scope.save = function(competitor) {
		saveInProgress(true);

		var data = angular.copy(competitor);
		if(competitor.team) {
			data.teamGuid = competitor.team.guid;
		}
		delete data.team;
		delete data.confirmPassword;

		competitorService.create(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			competitorApi.goToList();
			saveInProgress(false);
		});
	};

	$scope.competitorLoginIsFree = function(login) {
		return competitorService.loginIsFree(login);
	};

	$scope.refreshTeams = function (search) {
	    return teamService.searchTeamByAnyMatchingParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableTeams = [ $scope.noTeam ];
  				response.data.forEach(function(entry) {
      	  			$scope.availableTeams.push(entry);
  				});
			  }
	      });
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
