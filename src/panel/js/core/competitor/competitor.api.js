app.constant("COMPETITOR_API_CONSTANTS", {
    list: "/tournament/competitor",
    create: "/tournament/competitor/create",
    update: "/tournament/competitor/details/:competitorGuid"
});

app.service("competitorApi", ["$location", "COMPETITOR_API_CONSTANTS", function($location, COMPETITOR_API_CONSTANTS) {
    this.goToList = function() {
        $location.path(COMPETITOR_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(COMPETITOR_API_CONSTANTS.create);
    };

    this.goToUpdate = function(competitorGuid) {
        $location.path(COMPETITOR_API_CONSTANTS.update.replace(":competitorGuid", competitorGuid));
    };
}]);
