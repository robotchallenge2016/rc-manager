app.service("competitorService", ["$http", "$q", "contextService", function ($http, $q, contextService) {

	this.create = function(competitor) {
		var url = "/competitor";
		return $http.post(contextService.insertTournamentEditionContext(url), competitor);
	};

	this.update = function(competitor) {
		var url = "/competitor/guid/" + competitor.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), competitor);
	};

	this.delete = function(guid) {
		var url = "/competitor/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.promote = function(guid) {
		var url = "/competitor/promote/guid/" + guid;
		return $http.put(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/competitor/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/competitor/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.loginIsFree = function(login) {
		var url = "/competitor/login/" + login + "/free";
		if(login !== null) {
			return $http.get(contextService.insertTournamentEditionContext(url));
		}
		return $q.when(true);
	};

	this.searchMatchingAnyParam = function(search) {
		if(search.length > 2) {
			var params = {search: search};
			var url = "/competitor/match/any";
			return $http.get(contextService.insertTournamentEditionContext(url), {params: params});
		}
		return $q.when(null);
	};

	this.searchUnconfirmedCompetitorMatchingAnyParam = function(search) {
		if(search.length > 2) {
			var params = {search: search};
			var url = "/competitor/unconfirmed";
			return $http.get(contextService.insertTournamentEditionContext(url), {params: params});
		}
		return $q.when(null);
	};

	this.searchEmptyTeamMatchingAnyParam = function(search) {
		if(search.length > 2) {
			var params = {search: search};
			var url = "/competitor/team/empty/any";
			return $http.get(contextService.insertTournamentEditionContext(url), {params: params});
		}
		return $q.when(null);
	};

	this.searchTeamMembers = function(teamGuid) {
		var url = "/competitor/team/" + teamGuid + "/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.searchRobotOwner = function(robotGuid) {
		var url = "/competitor/robot/" + robotGuid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};
}]);
