app.controller("updateCompetitorDetailsCtrl",
	["$scope", "$routeParams", "$q", "competitorService", "competitorApi", "teamService", "robotService", "alertService",
	function ($scope, $routeParams, $q, competitorService, competitorApi, teamService, robotService, alertService) {

	var competitor = {
		guid: $routeParams.competitorGuid
	};

	$scope.competitorApi = competitorApi;
	$scope.competitor = competitor;
	$scope.memento = {};

	var noTeam = { name: "Brak", guid: null };
	$scope.noTeam = noTeam;
	$scope.availableTeams = [ noTeam ];

	// loader icons
	$scope.saveInProgress = false;

	$scope.removeTeam = function() {
		$scope.competitor.team = null;
		$scope.competitorForm.selectedTeam.$setDirty();
	};

	$scope.save = function(competitor) {
		saveInProgress(true);

		var data = angular.copy(competitor);
		if(competitor.team) {
			data.teamGuid = competitor.team.guid;
		}
		delete data.team;
		delete data.robots;
		delete data.confirmPassword;

		competitorService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			competitorApi.goToList();
			saveInProgress(false);
		});
	};

	competitorService.getByGuid(competitor.guid)
		.then(function(result) {
			var competitor = result.data;
			competitor.team = $scope.competitor.team;
			competitor.robots = $scope.competitor.robots;
			$scope.competitor = competitor;
			$scope.memento.competitor = angular.copy($scope.competitor);
		});

	teamService.getByCompetitorGuid(competitor.guid)
		.then(function(result) {
			$scope.competitor.team = result.data;
		});

	robotService.getByCompetitorGuid(competitor.guid)
		.then(function(result) {
			$scope.competitor.robots = result.data;
		});

	$scope.refreshTeams = function (search) {
	    return teamService.searchTeamByAnyMatchingParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableTeams = [ $scope.noTeam ];
				response.data.forEach(function(entry) {
    	  			$scope.availableTeams.push(entry);
				});
			  }
	      });
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
