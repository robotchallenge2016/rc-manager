app.config(["$routeProvider", "COMPETITOR_API_CONSTANTS", function ($routeProvider, COMPETITOR_API_CONSTANTS) {
	$routeProvider
    // Competitor page
	.when(COMPETITOR_API_CONSTANTS.list, {
		templateUrl: "views/core/competitor/competitor-list.view.html",
		controller: "competitorListCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(COMPETITOR_API_CONSTANTS.create, {
		templateUrl: "views/core/competitor/competitor-create-details.view.html",
		controller: "createCompetitorDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(COMPETITOR_API_CONSTANTS.update, {
		templateUrl: "views/core/competitor/competitor-update-details.view.html",
		controller: "updateCompetitorDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
