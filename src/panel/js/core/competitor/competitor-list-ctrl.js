app.controller("competitorListCtrl",
	["$scope", "$location", "$uibModal", "systemUserService", "competitorService", "competitorApi", "alertService",
	function ($scope, $location, $uibModal, systemUserService, competitorService, competitorApi, alertService) {

	$scope.competitorApi = competitorApi;
	$scope.competitors = [];

	$scope.open = function (competitor) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/competitor/competitor-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return competitor;
	      	}
	      }
	    });

		removeModal.result
		.then(function (competitor) {
			$scope.delete(competitor.guid);
	    });
	};

	$scope.delete = function(guid) {
		competitorService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		});
	};

	$scope.getAll = function() {
		competitorService.getAll()
		.then(function(result) {
			$scope.competitors = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista uczestników nie została pobrana !");
		});
	};

	$scope.markAsVerified = function(systemUser) {
		systemUserService.markAsVerified(systemUser)
		.then(function(result) {
			alertService.success("Uzytkownik został aktywowany !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Nie udało się aktywwać użytkownika !");
		});
	};

	$scope.markAsUnverified = function(systemUser) {
		systemUserService.markAsUnverified(systemUser)
		.then(function(result) {
			alertService.success("Uzytkownik został zablokowany !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Nie udało się zablokować użytkownika !");
		});
	};

	$scope.promoteToUser = function(systemUser) {
		competitorService.promote(systemUser.guid)
		.then(function(result) {
			alertService.success("Uzytkownik został promowany do rangi USER !");
			$scope.getAll();
		});
	};

	$scope.getAll();
}]);
