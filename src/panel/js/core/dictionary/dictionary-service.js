app.service("dictionaryService", ["$http", "$q", "dictionaryLoadedEventService", "contextService",
    function($http, $q, dictionaryLoadedEventService, contextService) {

    var self = this;
	this.systemRoles = [];
    this.competitionTypes = [];
    this.competitionSubtypes = [];
    this.robotTypes = [];

    this.LINEFOLLOWER_COMPETITION = "LINEFOLLOWER";
    this.SUMO_COMPETITION = "SUMO";
    this.FREESTYLE_COMPETITION = "FREESTYLE";

    this.retrieveDictionaries = function() {
    	retrieveSystemRoles();
        retrieveCompetitionTypes();
        retrieveRobotTypes();
        retrieveCompetitionSubtypes();

        return $q.all([
    		retrieveSystemRoles(),
    		retrieveCompetitionTypes(),
            retrieveRobotTypes(),
            retrieveCompetitionSubtypes()
    	]).then(function(result) {
    		dictionaryLoadedEventService.broadcast();
    	});
    };

    this.systemRoleByName = function(name) {
    	return this.systemRoles.filter(function(systemRole) {
    		return systemRole.name == name;
    	})[0];
    };

    this.systemRoleByGuid = function(guid) {
    	return this.systemRoles.filter(function(systemRole) {
    		return systemRole.guid == guid;
    	})[0];
    };

	function retrieveSystemRoles() {
		var url = "/system/dictionary/role/all";
 		return $http.get(contextService.insertTournamentEditionContext(url),
 		{
 			cache : true
 		})
		.then(function (result) {
			self.systemRoles = result.data;
		});
    }

    this.competitionTypeByGuid = function(guid) {
    	return this.competitionTypes.filter(function(competitionType) {
    		return competitionType.guid == guid;
    	})[0];
    };

    function retrieveCompetitionTypes() {
    	var url = "/dictionary/competition/type/all";
 		return $http.get(contextService.insertTournamentEditionContext(url),
 		{
 			cache : true
 		})
		.then(function (result) {
			self.competitionTypes = result.data;
		});
    }

    this.competitionSubtypeByCompetitionType = function(competitionType) {
        return this.competitionSubtypes.filter(function(competitionSubtype) {
            return competitionSubtype.competitionType.name == competitionType;
        });
    };

    this.competitionSubtypeByRobotType = function(robotType) {
        return this.competitionSubtypes.filter(function(competitionSubtype) {
            return competitionSubtype.robotType.guid == robotType.guid;
        })[0];
    };

    this.competitionSubtypeByGuid = function(guid) {
    	return this.competitionSubtypes.filter(function(competitionSubtype) {
    		return competitionSubtype.guid == guid;
    	})[0];
    };

    function retrieveCompetitionSubtypes() {
    	var url = "/dictionary/competition/subtype/all";
 		return $http.get(contextService.insertTournamentEditionContext(url),
 		{
 			cache : true
 		})
		.then(function (result) {
			self.competitionSubtypes = result.data;
		});
    }

    this.robotTypeByGuid = function(guid) {
    	return this.robotTypes.filter(function(robotType) {
    		return robotType.guid == guid;
    	})[0];
    };

    function retrieveRobotTypes() {
    	var url = "/dictionary/robot/type/all";
 		return $http.get(contextService.insertTournamentEditionContext(url),
 		{
 			cache : true
 		})
		.then(function (result) {
			self.robotTypes = result.data;
		});
    }
}]);
