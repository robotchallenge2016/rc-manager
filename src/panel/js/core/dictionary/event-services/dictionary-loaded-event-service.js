app.service("dictionaryLoadedEventService", ["$rootScope", "DICTIONARY_EVENTS", function ($rootScope, DICTIONARY_EVENTS) {
    this.broadcast = function() {
    	$rootScope.$broadcast(DICTIONARY_EVENTS.loaded);
    };

    this.listen = function(callback) {
    	$rootScope.$on(DICTIONARY_EVENTS.loaded, callback);
    };
}]);
