app.controller("createTeamDetailsCtrl",
	["$scope", "alertService", "teamService", "teamApi", "competitorService",
	function ($scope, alertService, teamService, teamApi, competitorService) {

	$scope.teamApi = teamApi;
	$scope.team = {
		competitors: []
	};

	// loader icons
	$scope.saveInProgress = false;

	$scope.save = function(team) {
		saveInProgress(true);

		var data = angular.copy(team);
		data.captainGuid = data.captain.guid;
		data.competitorGuids = [];
		for (var i = 0; i < team.competitors.length; i++) {
			data.competitorGuids[i] = team.competitors[i].guid;
		}
		delete data.captain;
		delete data.competitors;

		teamService.create(data)
		.then(function() {
			alertService.success("Zapisano !");
			teamApi.goToList();
			saveInProgress(false);
		})
		.catch(function() {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	$scope.teamNameIsFree = function(teamName) {
		return teamService.nameIsFree(teamName);
	};

	$scope.refreshCompetitors = function (search) {
	    return competitorService.searchEmptyTeamMatchingAnyParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableCompetitors = response.data;
			  }
	      });
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
