app.config(["$routeProvider", "TEAM_API_CONSTANTS", function ($routeProvider, TEAM_API_CONSTANTS) {
	$routeProvider

	.when(TEAM_API_CONSTANTS.list, {
		templateUrl: "views/core/team/team-list.view.html",
		controller: "teamListCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(TEAM_API_CONSTANTS.create, {
		templateUrl: "views/core/team/team-create-details.view.html",
		controller: "createTeamDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	})
	.when(TEAM_API_CONSTANTS.update, {
		templateUrl: "views/core/team/team-update-details.view.html",
		controller: "updateTeamDetailsCtrl",
		authz: {
			isAuthorized: "ADMIN"
		}
	});
}]);
