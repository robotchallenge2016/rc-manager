app.controller("teamListCtrl", ["$scope", "$uibModal", "teamService", "teamApi", "alertService",
    function ($scope, $uibModal, teamService, teamApi, alertService) {

    $scope.teamApi = teamApi;
	$scope.teams = [];

	$scope.open = function (team) {

		var removeModal = $uibModal.open({
	      templateUrl: "views/core/team/team-remove.view.html",
	      controller: "entityRemoveCtrl",
	      resolve: {
	      	entity: function () {
	      		return team;
	      	}
	      }
	    });

		removeModal.result
		.then(function (team) {
			$scope.delete(team.guid);
	    });
	};

	$scope.delete = function(guid) {
		teamService.delete(guid)
		.then(function(result) {
			alertService.success("Usunięto !");
			$scope.getAll();
		})
		.catch(function(error) {
			alertService.warn("Zespół nie został usunięty !");
		});
	};

	$scope.getAll = function() {
		teamService.getAll()
		.then(function(result) {
			$scope.teams = result.data;
		})
		.catch(function(error) {
			alertService.warn("Lista zespołów nie została pobrana !");
		});
	};

	$scope.getAll();
}]);
