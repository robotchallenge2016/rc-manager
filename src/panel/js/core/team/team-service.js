app.service("teamService", ["$http", "$q", "contextService", function ($http, $q, contextService) {

	this.create = function(team) {
		var url = "/team";
		return $http.post(contextService.insertTournamentEditionContext(url), team);
	};

	this.update = function(team) {
		var url = "/team/guid/" + team.guid;
		return $http.put(contextService.insertTournamentEditionContext(url), team);
	};

	this.delete = function(guid) {
		var url = "/team/guid/" + guid;
		return $http.delete(contextService.insertTournamentEditionContext(url));
	};

	this.getAll = function() {
		var url = "/team/all";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByGuid = function(guid) {
		var url = "/team/guid/" + guid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.getByCompetitorGuid = function(competitorGuid) {
		var url = "/team/competitor/guid/" + competitorGuid;
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.nameExists = function(teamName) {
		var url = "/team/name/" + teamName + "/exists";
		return $http.get(contextService.insertTournamentEditionContext(url));
	};

	this.nameIsFree = function(teamName) {
		if(teamName !== null) {
			var url = "/team/name/" + teamName + "/free";
			return $http.get(contextService.insertTournamentEditionContext(url));
		}
		return $q.when(true);
	};

	this.searchTeamByAnyMatchingParam = function(search) {
		if(search.length > 2) {
			var params = {search: search};
            var url = "/team/match/any";
			return $http.get(contextService.insertTournamentEditionContext(url), {params: params});
		}
		return $q.when(null);
	};
}]);
