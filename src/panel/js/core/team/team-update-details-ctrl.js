app.controller("updateTeamDetailsCtrl",
	["$scope", "$routeParams", "$q", "teamService", "teamApi", "competitorService", "alertService",
	function ($scope, $routeParams, $q, teamService, teamApi, competitorService, alertService) {

	$scope.teamApi = teamApi;
	$scope.team = {
		guid: $routeParams.teamGuid,
		competitiors: []
	};
	$scope.memento = {};

	// loader icons
	$scope.saveInProgress = false;

	$scope.save = function(team) {
		saveInProgress(true);

		var data = angular.copy(team);
		data.competitorGuids = [];
		for (var i = 0; i < team.competitors.length; i++) {
			data.competitorGuids[i] = team.competitors[i].guid;
		}
		delete data.competitors;

		teamService.update(data)
		.then(function(result) {
			alertService.success("Zapisano !");
			teamApi.goToList();
			saveInProgress(false);
		})
		.catch(function(error) {
			alertService.warn("Zmiany nie zostały zapisane !");
			saveInProgress(false);
		});
	};

	$scope.teamNameIsFree = function(teamName) {
		if(teamName != $scope.memento.team.name) {
			return teamService.nameIsFree(teamName);
		}
		return $q.when(true);
	};

	$q.all([
		teamService.getByGuid($scope.team.guid),
		competitorService.searchTeamMembers($scope.team.guid)
	]).then(function(result) {
		$scope.team = result[0].data;
		$scope.memento.team = angular.copy($scope.team);
		$scope.team.competitors = result[1].data;
		for(var i = 0; i < $scope.team.competitors.length; ++i) {
			var competitor = $scope.team.competitors[i];
			if(competitor.captainOfTeam) {
				$scope.captain = competitor;
				$scope.captain.fullName = competitor.firstName + " " + competitor.lastName;
				$scope.team.competitors.splice(i, 1);
				break;
			}
		}
	});

	$scope.refreshCompetitors = function (search) {
	    return competitorService.searchEmptyTeamMatchingAnyParam(search)
	      .then(function(response) {
			  if(response !== null) {
			  	$scope.availableCompetitors = response.data;
			  }
	      });
	};

	function saveInProgress(show) {
		$scope.saveInProgress = show;
	}
}]);
