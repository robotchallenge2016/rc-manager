app.constant("TEAM_API_CONSTANTS", {
    list: "/tournament/team",
    create: "/tournament/team/create",
    update: "/tournament/team/details/:teamGuid"
});

app.service("teamApi", ["$location", "TEAM_API_CONSTANTS", function($location, TEAM_API_CONSTANTS) {
    this.goToList = function() {
        $location.path(TEAM_API_CONSTANTS.list);
    };

    this.goToCreate = function() {
        $location.path(TEAM_API_CONSTANTS.create);
    };

    this.goToUpdate = function(teamGuid) {
        $location.path(TEAM_API_CONSTANTS.update.replace(":teamGuid", teamGuid));
    };
}]);
