var app = angular.module("app", [
    "ngRoute", "ngAnimate", "ngSanitize", "angular-loading-bar",
    "ui.bootstrap", "ui.select", "ui.validate", "ui.tinymce",
    "ui.grid", "ui.grid.selection"]);

app.config(["$httpProvider", function ($httpProvider) {
    $httpProvider.interceptors.push([ "$injector", function ($injector) {
            return $injector.get("authnInterceptor");
        }
    ]);

    $httpProvider.interceptors.push([ "$injector", function ($injector) {
            return $injector.get("authzInterceptor");
        }
    ]);

    $httpProvider.interceptors.push([ "$injector", function ($injector) {
            return $injector.get("applicationErrorInterceptor");
        }
    ]);
}]);

app.config(["uiSelectConfig", function(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
}]);

app.run(["$injector", function ($injector) {
      $injector.get('eventRoutingService');
      $injector.get('dictionaryService');
}]);
