app.controller("panelController",
    ["$scope", "$q", "$location", "$route", "$timeout", "$window", "sessionService", "teamService", "authnService", "authzService", "contextService", "dictionaryService", "alertService",
 	function ($scope, $q, $location, $route, $timeout, $window, session, teamService, authnService, authzService, contextService, dictionaryService, alertService) {
    // Preloader
    $scope.isInitialized = false;
    // AuthN
	$scope.isAuthenticated = false;
    // AuthZ
	$scope.isAdmin = false;
	$scope.isUser = false;
	$scope.isCompetitor = false;

	$scope.alerts = alertService.alerts();
	$scope.closeAlert = alertService.closeAlert;

	$scope.context = contextService.context;

    $q.all([
		session.introspection(),
        contextService.retrieveContext(),
        contextService.retrieveEditionsContext()
	])
    .then(function(result) {
        $scope.context.tournament = result[1].data;
        $scope.context.editions = result[2].data;
        if($scope.context.editions.length > 0) {
        	$scope.context.selectedEdition = $scope.context.editions[0];
		}
		loadDictionaries();
	});

    function loadDictionaries() {
        dictionaryService.retrieveDictionaries()
            .then(function(result) {
                initialize();
            });
	}

	function initialize() {
        $scope.systemUser = session.systemUser;
        $scope.username = $scope.systemUser.login.split("@")[0];
		$scope.isAuthenticated = true;
		$scope.isAdmin = authzService.isAuthorized('ADMIN');
		$scope.isUser = authzService.isAuthorized('USER');
		$scope.isCompetitor = authzService.isAuthorized('COMPETITOR');
	}

    $scope.appReady = function() {
        $scope.isInitialized = true;
    };

	$scope.selectEdition = function(edition) {
		$scope.context.selectedEdition = edition;
		$route.reload();
	};

    $scope.logout = function() {
        authnService.logout()
            .then(function() {
                alertService.success("Wylogowanie powiodło się!");
                $timeout(function() {
                    $window.location.href = '/panel/logout/';
                }, 250);
            })
            .catch(function() {
                alertService.warn("Wylogowanie nie powiodło się!");
            });
    };

    $scope.getClass = function (path) {
		if($location.path().substr(0, path.length) == path) {
			return "active";
		} else {
			return "";
		}
	};

    $scope.goBack = function() {
        window.history.back();
    };
}]);
